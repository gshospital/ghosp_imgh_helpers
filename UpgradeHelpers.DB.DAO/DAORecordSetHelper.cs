using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using System.Data.OracleClient;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text.RegularExpressions;
using System.Xml;
using UpgradeHelpers.DB;

namespace UpgradeHelpers.DB.DAO
{

    /// <summary>
    /// Support class for the DAO.Recorset the object that represents the records in a base table or the records that result from running a query.
    /// </summary>
    [Serializable]
    public class DAORecordSetHelper : RecordSetHelper, ISerializable
    {
        #region Class Variables
        /// <summary>Lock editing operations for the current object.</summary>
        private EditModeEnum _editMode = EditModeEnum.EditNone;
        /// <summary>Indicates the result of a seek or find operation.</summary>
        private bool _noMatch;
        /// <summary>Indicates the type of this recordset.</summary>
        private DAORecordsetTypeEnum _daoRsType = DAORecordsetTypeEnum.dbOpenDynamic; //default
        /// <summary>Indicates the options for this recorset.</summary>
        private DAORecordsetOptionEnum _daoRsOption;
        /// <summary>Indicates the lock for this recordset.</summary>
        private DAOLockTypeEnum _daoLockType = DAOLockTypeEnum.dbOptimistic; //default

        private List<KeyValuePair<bool, object>> _defaultValues; //isComputed - value

        /// <summary>
        /// Internal variable added to indicate that the recordset is disconnected
        /// </summary>
        protected bool Disconnected = false;

        private Dictionary<KeyValuePair<DbConnection, string>, DbDataAdapter> _dataAdaptersCached = new Dictionary<KeyValuePair<DbConnection, string>, DbDataAdapter>();

        //[NonSerialized]
        private DbConnection _activeConnection;

        /// <summary>
        /// New Datarow view when adding to a sorted or filtered collection
        /// </summary>
        private DataRowView _dbvRow = null;

        /// <summary>
        /// Connection String
        /// </summary>
        private String _connectionString = string.Empty;

        /// <summary>
        /// string for update query
        /// </summary>
        private String _sqlUpdateQuery = string.Empty;

        /// <summary>
        /// string for delete query
        /// </summary>
        private String _sqlDeleteQuery = string.Empty;

        /// <summary>
        /// string for insert query
        /// </summary>
        private String _sqlInsertQuery = string.Empty;

        /// <summary>
        /// operation finished state
        /// </summary>
        private bool _operationFinished;

        /// <summary>
        /// is filtered?
        /// </summary>
        private bool _filtered;

        /// <summary>
        /// is first End Of File?
        /// </summary>
        private bool _firstEof = true;

        /// <summary>
        /// is first change?
        /// </summary>
        private bool _firstChange = true;

        /// <summary>
        /// is deserilized
        /// </summary>
        private bool _isDeserialized;

        private bool _cachingAdapter;

        private bool _requiresDefaultValues;

        /// <summary>
        /// has auto increment columns
        /// </summary>
        private bool _isDefaultSerializationInProgress;

        /// <summary>
        /// has auto increment columns
        /// </summary>
        private bool _hasAutoincrementCols;

        /// <summary>
        /// auto increment column name
        /// </summary>
        private string _autoIncrementCol = String.Empty;

        /// <summary>
        /// actual Connection State
        /// </summary>
        private ConnectionState _connectionStateAtEntry = ConnectionState.Closed;




        private const string TITLE_DIALOG_RecordSetError = "RecordSet Error";

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new DAORecordSet instance using the default factory specified on the configuration xml.
        /// </summary>
        public DAORecordSetHelper()
            : this("")
        {
        }

        /// <summary>
        /// Creates a new DAORecordSet instance using the factory specified on the �factoryName� parameter.
        /// </summary>
        /// <param name="factory">The name of the factory to by use by this DAORecordsetHelper object (the name most exist on the configuration xml file).</param>
        public DAORecordSetHelper(DbProviderFactory factory)
        {
            IsDefaultSerializationInProgress = false;
            _providerFactory = factory;
        }

        /// <summary>
        /// Creates a new DAORecordSet instance using the factory specified on the �factoryName� parameter.
        /// </summary>
        /// <param name="factoryName">The name of the factory to by use by this DAORecordsetHelper object (the name most exist on the configuration xml file).</param>
        public DAORecordSetHelper(String factoryName)
            : this(AdoFactoryManager.GetFactory(factoryName))
        {
            IsDefaultSerializationInProgress = false;
            _index = -1;
            _newRow = false;
            DatabaseType = AdoFactoryManager.GetFactoryDbType(factoryName);
        }

        /// <summary>
        /// Creates a new DAORecordSet instance using the factory specified on the �factoryName� and the configuration provided by the other parameters.
        /// </summary>
        /// <param name="factoryName">The name of the factory to by use by this DAORecordsetHelper object (the name most exist on the configuration xml file).</param>
        /// <param name="rsType">The DAORecordsetTypeEnum of this DAORecordsetHelper object.</param>
        /// <param name="rsOption">The DAORecordsetOptionEnum of this DAORecordsetHelper object.</param>
        /// <param name="lockType">The DAOLockTypeEnum of this DAORecordsetHelper object.</param>
        public DAORecordSetHelper(String factoryName, DAORecordsetTypeEnum rsType, DAORecordsetOptionEnum rsOption, DAOLockTypeEnum lockType)
            : this(AdoFactoryManager.GetFactory(factoryName))
        {
            _daoRsType = rsType;
            _daoRsOption = rsOption;
            _daoLockType = lockType;
        }
        /// <summary>
        /// 
        /// </summary>
        protected DAORecordSetHelper(
         SerializationInfo info, StreamingContext context)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        [SecurityPermission(SecurityAction.Demand,
            SerializationFormatter = true)]
        public override void GetObjectData(
            SerializationInfo info, StreamingContext context)
        {

            info.AddValue("Index", _index);
            info.AddValue("NewRow", _newRow);
            // info.AddValue("Filtered", _filtered);
            info.AddValue("Opened", _opened);
            info.AddValue("FirstEof", _firstEof);
            info.AddValue("FirstChange", _firstChange);
            info.AddValue("EOF", _eof);
            info.AddValue("FactoryName", FactoryName);
            info.AddValue("RecordSource", RecordSource);
            info.AddValue("ActiveConnectionWasNull", ActiveConnection == null);
            info.AddValue("ConnectionString", ConnectionString);
        }
        /// <summary>
        /// Gets Command Source query string
        /// </summary>
        /// <returns>string</returns>
        public String GetSource()
        {
            DbCommand command = _source as DbCommand;
            if (command != null)
            {
                return command.CommandText;
            }
            return (String)_source;
        }

        /// <summary>
        /// Converts from DbType to System.Type.
        /// </summary>
        /// <param name="dbType">The DBType to be converted.</param>
        /// <returns>The equivalent System.Type.</returns>
        public static Type GetType(DbType dbType)
        {
            Type result = System.Type.GetType("System.String");
            switch (dbType)
            {
                case DbType.Byte:
                    result = System.Type.GetType("System.Byte");
                    break;
                case DbType.Binary:
                    result = System.Type.GetType("System.Byte[]");
                    break;
                case DbType.Boolean:
                    result = System.Type.GetType("System.Boolean");
                    break;
                case DbType.DateTime:
                    result = System.Type.GetType("System.DateTime");
                    break;
                case DbType.Decimal:
                    result = System.Type.GetType("System.Decimal");
                    break;
                case DbType.Double:
                    result = System.Type.GetType("System.Double");
                    break;
                case DbType.Guid:
                    result = System.Type.GetType("System.Guid");
                    break;
                case DbType.Int16:
                    result = System.Type.GetType("System.Int16");
                    break;
                case DbType.Int32:
                    result = System.Type.GetType("System.Int32");
                    break;
                case DbType.Int64:
                    result = System.Type.GetType("System.Int64");
                    break;
                case DbType.Object:
                    result = System.Type.GetType("System.Object");
                    break;
                case DbType.SByte:
                    result = System.Type.GetType("System.SByte");
                    break;
                case DbType.Single:
                    result = System.Type.GetType("System.Single");
                    break;
                case DbType.String:
                    result = System.Type.GetType("System.String");
                    break;
                case DbType.UInt16:
                    result = System.Type.GetType("System.UInt16");
                    break;
                case DbType.UInt32:
                    result = System.Type.GetType("System.UInt32");
                    break;
                case DbType.UInt64:
                    result = System.Type.GetType("System.UInt64");
                    break;
            }

            return result;

        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string FactoryName
        {
            get
            {
                if (ProviderFactory == null) return String.Empty;
                return AdoFactoryManager.GetFactoryNameFromProviderType(ProviderFactory.GetType());
            }
        }

        /// <summary>
        /// Gets a string with the SQL query being use to obtain the RecordsetHelper data.
        /// </summary>
        public string RecordSource
        {
            set { _sqlSelectQuery = value; }
            get
            {
                return _sqlSelectQuery;
            }
        }

        /// <summary>
        /// Gets an active connection
        /// </summary>
        public override DbConnection ActiveConnection
        {
            get
            {
                return _activeConnection;
            }
            set
            {
                if (value != null)
                    Validate();
                if (_activeCommand != null)
                {
                    _activeCommand.Connection = value;
                }
                _activeConnection = value;
                _connectionString = ActiveConnection != null ? ActiveConnection.ConnectionString : String.Empty;
                _connectionStateAtEntry = ActiveConnection != null ? ActiveConnection.State : ConnectionState.Closed;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override RecordSetHelper CreateInstance()
        {
            return new DAORecordSetHelper();
        }

        /// <summary>
        /// Gets or sets the connection string being use by this RecordsetHelper object.
        /// </summary>
        public override String ConnectionString
        {
            get
            {
                return _connectionString;
            }
            set
            {
                _connectionString = value;
                if (_providerFactory != null)
                {
                    try
                    {
                        Validate();
#if TargetF2
                        DbConnection connection = DBTrace.CreateConnectionWithTrace(_providerFactory);
#else
                        DbConnection connection = _providerFactory.CreateConnection();
                        //DbConnection connection =  providerFactory.CreateConnectionWithTrace();
#endif
                        if (connection != null)
                        {
                            connection.ConnectionString = value;
                            ActiveConnection = connection;
                        }
#if TargetF2
                        DBTrace.OpenWithTrace(ActiveConnection);
#else
                        //ActiveConnection.OpenWithTrace();
                        ActiveConnection.Open();
#endif
                    } // try
                    catch (Exception ex)
                    {
                        String message = string.Format(
                                    "Problem while trying to set the active connection. Please verify ConnectionString {0} and Factory {1} settings. Error details {2}",
                                    _connectionString,
                                    _providerFactory,
                                    ex.Message);
#if WINFORMS
                        if (Process.GetCurrentProcess().ProcessName == "devenv")
						{
							System.Windows.Forms.MessageBox.Show(message,TITLE_DIALOG_RecordSetError);
						} // if
#else
                        Trace.TraceError(message);
#endif
                        if (!Disconnected)
                            throw;
                    } // catch
                }
                _defaultValues = null;
            }
        }


        /// <summary>
        /// Gets and sets a bookmark to an specific record inside the ADORecordsetHelper.
        /// </summary>
        public DataRow Bookmark
        {
            get
            {
                return UsingView ? _currentView[_index].Row : Tables[CurrentRecordSet].Rows[_index];
            }
            set
            {
                CancelUpdate();
                _index = FindBookmarkIndex(value);
            }
        }

        /// <summary>
        /// Finds the index in the RecordsetHelper for the �value�.
        /// </summary>
        /// <param name="value">The DataRow to look for.</param>
        /// <returns>The index number if is found, otherwise -1.</returns>
        protected int FindBookmarkIndex(DataRow value)
        {
            if (!UsingView)
            {
                return Tables[CurrentRecordSet].Rows.IndexOf(value);
            }
            int result = -1;
            for (int i = 0; i < _currentView.Count; i++)
            {
                if (_currentView[i].Row == value)
                {
                    result = i;
                    break;
                }
            }
            return result;
        }

        /// <summary>
        /// Property used to determine if the data needs to be get from a dataview or the table directly
        /// </summary>
        protected override bool UsingView
        {
            get
            {
                return Filtered;
            }
        }

        /// <summary>
        /// Return true if the recordsethelper is caching the adapters
        /// </summary>
        public bool IsCachingAdapter
        {
            get { return _cachingAdapter; }
        }

        /// <summary>
        /// Gets and Sets the position of the current record on the recordset instance.
        /// </summary>
        public int AbsolutePosition
        {
            get
            {
                return _index;
            }
            set
            {
                BasicMove(value);
            }
        }

        /// <summary>
        /// Gets or Sets if lock is in effect while editing.  
        /// </summary>
        public bool LockEdits
        {
            get
            {
                return _editMode == EditModeEnum.EditNone;
            }
            set
            {
                _editMode = value ? EditModeEnum.EditInProgress : EditModeEnum.EditNone;
            }
        }

        /// <summary>
        ///     Returns a new recordset according to the compound statement on the current recordset
        /// </summary>
        /// <returns>A new open recordset</returns>
        public DAORecordSetHelper NextRecordSet()
        {
            DAORecordSetHelper result = null;
            if (CurrentRecordSet < Tables.Count - 1)
            {
                CurrentRecordSet++;
                result = this;
            }
            else
            {
                this.Close();
            }
            return result;
        }

        /// <summary>
        /// Indicates whether a particular record was found by using the Seek method or one of the Find methods.
        /// </summary>
        public bool NoMatch
        {
            get
            {
                return _noMatch;
            }
        }

        /// <summary>
        /// Gets or sets the type for this DAORecordSetHelper object.
        /// </summary>
        public DAORecordsetTypeEnum Type
        {
            get
            {
                return _daoRsType;
            }
            set
            {
                _daoRsType = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override bool CanMoveNext
        {
            get { return !EOF; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Cancels execution of any pending process.
        /// </summary>
        public void Cancel()
        {
            DoCancelUpdate();
        }
        /// <summary>
        /// Cancels a pending batch update.
        /// </summary>
        public void CancelBatch()
        {
            Tables[CurrentRecordSet].RejectChanges();
            _newRow = false;
        }

        /// <summary>
        /// Verifies if a parameter with the provided name exists on the command received, otherwise a new parameter using the specified name.
        /// </summary>
        /// <param name="command">The command object to look into.</param>
        /// <param name="name">The name of the parameter to look for.</param>
        /// <returns>The parameter named with �name�.</returns>
        public static DbParameter CommandParameterBinding(DbCommand command, string name)
        {
            if (!command.Parameters.Contains(name))
            {
                DbParameter parameter = command.CreateParameter();
                parameter.ParameterName = name;
                command.Parameters.Add(parameter);
            }
            return command.Parameters[name];
        }
        /// <summary>
        /// Cancels any changes made to the current or new row of a ADORecordsetHelper object.
        /// </summary>
        public void CancelUpdate()
        {
            DoCancelUpdate();
        }
        /// <summary>
        /// Creates a new record.
        /// </summary>
        public override void AddNew()
        {
            DoAddNew();
            _editMode = EditModeEnum.EditAdd;
        }

        /// <summary>
        /// Sets the recordset on edit mode.
        /// </summary>
        public void Edit()
        {
            _editMode = EditModeEnum.EditNone;
        }

        /// <summary>
        /// Returns a two dimmension array representing 'n' rows in a result set.
        /// </summary>
        /// <param name="numrows">Number of rows to be retrieved.</param>
        /// <returns>A delimited string containing a number of rows.</returns>
        public object[,] GetRows(int numrows)
        {
            object[,] buffer = new object[Tables[CurrentRecordSet].Columns.Count, numrows];
            int i = _index, colindex = 0, rowindex = 0;
            for (; !EOF && _index < i + numrows; _index++)
            {
                foreach (Object data in CurrentRow.ItemArray)
                {
                    buffer[colindex, rowindex] = data;
                    colindex++;
                }
                colindex = 0;
                rowindex++;
                _eof = _index >= Tables[CurrentRecordSet].Rows.Count - 1;
            }
            object[,] result = new object[Tables[CurrentRecordSet].Columns.Count, rowindex];
            for (int rindex = 0; rindex < rowindex; rindex++)
                for (int cindex = 0; cindex < Tables[CurrentRecordSet].Columns.Count; cindex++)
                    result[cindex, rindex] = buffer[cindex, rindex];
            return result;
        }

        /// <summary>
        /// Moves the position of the currentRecord in a RecordSet
        /// </summary>
        /// <param name="records">Amount of records positive or negative to move from the current record.</param>
        public void Move(int records)
        {
            BasicMove(_index + records);
        }
        /// <summary>
        /// Moves the position of the currentRecord in a RecordSet.
        /// </summary>
        /// <param name="rows">The number of rows the position will move. If rows is greater than 0, the position is moved forward (toward the end of the file). If rows is less than 0, the position is moved backward (toward the beginning of the file).</param>
        /// <param name="startBookmark">The start value to begin the move.</param>
        public void Move(int rows, DataRow startBookmark)
        {
            int tempBookmark = FindBookmarkIndex(startBookmark);
            _index = tempBookmark;
            Move(rows);
        }

        /// <summary>
        /// Locates the first record in DAORecordsetHelper object that satisfies the specified criteria and makes that record the current record.
        /// </summary>
        /// <param name="criteria">A String used to locate the record. It is like the WHERE clause in an SQL statement, but without the word WHERE.</param>
        public void FindFirst(string criteria)
        {
            _noMatch = true;
            // Save the current position. 

            // Apply the filter criteria to the data. 
            DataView result = Tables[CurrentRecordSet].DefaultView;
            result.RowFilter = criteria;

            if (result.Count > 0)
            {
                int tempIndex = FindBookmarkIndex(result[0].Row);
                _index = tempIndex;
                _noMatch = false;
            }
        }
        /// <summary>
        /// Gets a DataRow object containing the field values of the current record.
        /// </summary>
        public DataRow FieldsValues
        {
            get
            {
                return UsingView ? _currentView[_index].Row : Tables[CurrentRecordSet].Rows[_index];
            }
        }
        /// <summary>
        /// Looks in all records for a field that matches the �criteria�. 
        /// </summary>
        /// <param name="criteria">A String used to locate the record. It is like the WHERE clause in an SQL statement, but without the word WHERE.</param>
        public void Find(String criteria)
        {
            DataView result = Tables[CurrentRecordSet].DefaultView;
            result.RowFilter = criteria;
            if (result.Count > 0)
            {
                object[] values = result[0].Row.ItemArray;
                bool bfound = false;
                MoveFirst();
                while ((!bfound) && !EOF)
                {
                    for (int i = 0; i < values.Length; i++)
                    {
                        bfound = (CurrentRow.ItemArray[i].Equals(values[i]));
                        if (!bfound)
                        {
                            break;
                        }
                    }
                    if (!bfound)
                    {
                        MoveNext();
                    }
                }
            }
        }

        /// <summary>
        /// Sets a new value for a specific index column.
        /// </summary>
        /// <param name="columnIndex">Index of the column to be updated.</param>
        /// <param name="value">New value for column.</param>
        public void SetNewValue(int columnIndex, object value)
        {
            CurrentRow[columnIndex] = value;
        }

        /// <summary>
        /// Gets a DataColumnCollection object that contains the information of all columns on the RecordsetHelper.
        /// </summary>
        public DataColumnCollection FieldsMetadata
        {
            get
            {
                if (Tables.Count <= 0)
                {
                    Tables.Add();
                }
                return Tables[CurrentRecordSet].Columns;
            }
        }

        /// <summary>
        /// Gets or sets the row value at �ColumnIndex� index.
        /// </summary>
        /// <param name="columnIndex">index of the column to look for.</param>
        /// <returns>The value at the given index.</returns>
        public DaoFieldHelper GetField(int columnIndex)
        {
            DaoFieldHelper newField = new DaoFieldHelper(this, columnIndex, true);
            return newField;
        }
        /// <summary>
        /// Gets or sets the row value at �ColumnName� index.
        /// </summary>
        /// <param name="columnName">Name of the column to look for.</param>
        /// <returns>The value at the given index.</returns>
        public DaoFieldHelper GetField(String columnName)
        {
            DaoFieldHelper newField = new DaoFieldHelper(this, columnName, false);
            return newField;
        }

        /// <summary>
        /// Gets or sets the row value at �ColumnName� index.
        /// </summary>
        /// <param name="columnName">Name of the column to look for.</param>
        /// <returns>The value at the given index.</returns>
        public Object this[String columnName]
        {
            get
            {
                return CurrentRow[columnName];
            }
            set
            {
                int columnIndex = GetColumnIndexByName(columnName);
                if (columnIndex > -1)
                {
                    SetNewValue(columnIndex, value);
                }
                else
                {
                    throw new Exception(string.Format("Column {0} not found", columnName));
                }
            }
        }

        /// <summary>
        /// Gets or sets the row value at �ColumnIndex� index.
        /// </summary>
        /// <param name="columnIndex">index of the column to look for.</param>
        /// <returns>The value at the given index.</returns>
        public Object this[int columnIndex]
        {
            get
            {
                return CurrentRow[columnIndex];
            }
            set
            {
                SetNewValue(columnIndex, value);
            }
        }

        /// <summary>
        /// Looks in all records for a field that matches the �criteria�. 
        /// </summary>
        /// <param name="rowName">A String used to locate the row from the record.</param>
        /// <param name="pCriteria">A String used to locate the record. It is like the WHERE clause in an SQL statement, but without the word WHERE.</param>
        public void Find(String rowName, String pCriteria)
        {
            if (Tables[CurrentRecordSet].Rows.Count > 0)
            {
                bool bfound = false;
                MoveFirst();
                int i = 0;
                while ((!bfound) && !EOF)
                {
                    if (i < Tables[CurrentRecordSet].Rows.Count)
                    {
                        bfound = (Tables[CurrentRecordSet].Rows[i][rowName].Equals(pCriteria));
                    }
                    if (!bfound)
                    {
                        MoveNext();
                    }
                    i++;
                }
            }
        }
        /// <summary>
        /// Verifies if the ADORecordset object have been open.
        /// </summary>
        protected void Validate()
        {
        }

        /// <summary>
        /// Executes the atomic addNew Operation creating the new row and setting the newRow flag.
        /// </summary>
        protected virtual void DoAddNew()
        {
            if (UsingView)
            {
                _currentView.AddNew();
            }
            else
            {
                DataRow dbRow = Tables[CurrentRecordSet].NewRow();
                Tables[CurrentRecordSet].Rows.Add(dbRow);
                _index = Tables[CurrentRecordSet].Rows.Count - 1;
                _requiresDefaultValues = true;
            }
            _newRow = true;
        }

        /// <summary>
        /// Locates the last record in DAORecordsetHelper object that satisfies the specified criteria and makes that record the current record.
        /// </summary>
        /// <param name="criteria">A String used to locate the record. It is like the WHERE clause in an SQL statement, but without the word WHERE.</param>
        public void FindLast(string criteria)
        {
            _noMatch = true;
            // Save the current position. 

            // Apply the filter criteria to the data. 
            DataView result = Tables[CurrentRecordSet].DefaultView;
            result.RowFilter = criteria;

            if (result.Count > 0)
            {
                int tempIndex = FindBookmarkIndex(result[result.Count - 1].Row);
                _index = tempIndex;
                _noMatch = false;
            }
        }

        /// <summary>
        /// Returns the ActiveConnection object if it has been initialized otherwise creates a new DBConnection object.
        /// </summary>
        /// <param name="connectionString">The connection string to be used by the connection.</param>
        /// <returns>A DBConnection containing with the connection string set. </returns>
        protected virtual DbConnection GetConnection(String connectionString)
        {
            if (ActiveConnection != null && ActiveConnection.ConnectionString.Equals(connectionString, StringComparison.InvariantCultureIgnoreCase))
            {
                return ActiveConnection;
            }
#if TargetF2
            DbConnection connection = DBTrace.CreateConnectionWithTrace(_providerFactory);
#else
            //DbConnection connection = providerFactory.CreateConnectionWithTrace();
            DbConnection connection = _providerFactory.CreateConnection();
#endif
            if (connection != null)
            {
                connection.ConnectionString = connectionString;
                return connection;
            }
            return null;
        }

        /// <summary>
        /// Infers the command type from an SQL string getting the schema metadata from the database.
        /// </summary>
        /// <param name="sqlCommand">The sql string to be analyzed.</param>
        /// <param name="parameters">List of DbParameters</param>
        /// <returns>The command type</returns>
        internal override CommandType getCommandType(String sqlCommand, out List<DbParameter> parameters)
        {
            CommandType commandType = CommandType.Text;
            parameters = null;
            sqlCommand = sqlCommand.Trim();
            if (sqlCommand.StartsWith("select", StringComparison.InvariantCultureIgnoreCase) ||
                sqlCommand.StartsWith("insert", StringComparison.InvariantCultureIgnoreCase) ||
                sqlCommand.StartsWith("update", StringComparison.InvariantCultureIgnoreCase) ||
                sqlCommand.StartsWith("delete", StringComparison.InvariantCultureIgnoreCase) ||
                sqlCommand.StartsWith("exec", StringComparison.InvariantCultureIgnoreCase) ||
                sqlCommand.StartsWith("begin", StringComparison.InvariantCultureIgnoreCase))
            {
                commandType = CommandType.Text;
                return commandType;
            }
            string[] completeCommandParts = sqlCommand.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            sqlCommand = completeCommandParts[0];
            String[] commandParts = sqlCommand.Split(".".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            String objectQuery = String.Empty;
            DbConnection connection = GetConnection(_connectionString);
            if (connection.State != ConnectionState.Open)
            {
#if TargetF2
                    DBTrace.OpenWithTrace(connection);
#else
                //connection.OpenWithTrace();
                connection.Open();

#endif
            }
            DataRow[] existingObjects;
            DataTable dbObjects = connection.GetSchema("Tables");
            if (dbObjects.Rows.Count > 0)
            {
                //this is an sql server connection
                if (dbObjects.Columns.Contains("table_catalog") && dbObjects.Columns.Contains("table_schema"))
                {
                    if (commandParts.Length == 3)
                    {
                        objectQuery = "table_catalog = \'" + commandParts[0] + "\' AND table_schema = \'" + commandParts[1] + "\' AND table_name = \'" + commandParts[2] + "\'";
                    }
                    else if (commandParts.Length == 2)
                    {
                        objectQuery = "table_schema = \'" + commandParts[0] + "\' AND table_name = \'" + commandParts[1] + "\'";
                    }
                    else
                    {
                        objectQuery = "table_name = \'" + commandParts[0] + "\'";
                    }
                }
                else if (dbObjects.Columns.Contains("OWNER"))
                {
                    if (commandParts.Length == 2)
                    {
                        objectQuery = "OWNER = \'" + commandParts[0] + "\' AND TABLE_NAME = \'" + commandParts[1] + "\'";
                    }
                    else
                    {
                        objectQuery = "TABLE_NAME = \'" + commandParts[0] + "\'";
                    }
                }
                existingObjects = dbObjects.Select(objectQuery);
                if (existingObjects.Length > 0)
                {
                    commandType = CommandType.TableDirect;
                    return commandType;
                }
            }
            dbObjects = connection.GetSchema("Procedures");
            // The query for looking for stored procedures information is version sensitive.
            // The database version can be verified in SQLServer using a query like "Select @@version"
            // That version can be mapped to the specific SQL Server Product Version with a table like the provided here: http://www.sqlsecurity.com/FAQs/SQLServerVersionDatabase/tabid/63/Default.aspx 
            // The following code verifies columns for SQL Server version 2003, other versions might have a different schema and require changes
            if (dbObjects.Columns.Contains("procedure_catalog") && dbObjects.Columns.Contains("procedure_schema"))
            {
                if (commandParts.Length == 3)
                {
                    objectQuery = "procedure_catalog = \'" + commandParts[0] + "\' AND procedure_schema = \'" + commandParts[1] + " AND procedure_name = " + commandParts[2] +
                                  "\'";
                }
                else if (commandParts.Length == 2)
                {
                    objectQuery = "procedure_schema = \'" + commandParts[0] + "\' AND procedure_name = \'" + commandParts[1] + "\'";
                }
                else
                {
                    objectQuery = "procedure_name = \'" + commandParts[0] + "\'";
                }
            }
            else if (dbObjects.Rows.Count > 0)
            {
                //this is an sql server connection
                if (dbObjects.Columns.Contains("specific_catalog") && dbObjects.Columns.Contains("specific_schema"))
                {
                    if (commandParts.Length == 3)
                    {
                        objectQuery = "specific_catalog = \'" + commandParts[0] + "\' AND specific_schema = \'" + commandParts[1] + " AND specific_name = " + commandParts[2] +
                                      "\'";
                    }
                    else if (commandParts.Length == 2)
                    {
                        objectQuery = "specific_schema = \'" + commandParts[0] + "\' AND specific_name = \'" + commandParts[1] + "\'";
                    }
                    else
                    {
                        objectQuery = "specific_name = \'" + commandParts[0] + "\'";
                    }
                }
                else if (dbObjects.Columns.Contains("OWNER"))
                {
                    if (commandParts.Length == 2)
                    {
                        objectQuery = "OWNER = \'" + commandParts[0] + "\' AND OBJECT_NAME = \'" + commandParts[1] + "\'";
                    }
                    else
                    {
                        objectQuery = "OBJECT_NAME = \'" + commandParts[0] + "\'";
                    }
                }
                existingObjects = dbObjects.Select(objectQuery);
                if (existingObjects.Length > 0)
                {
                    commandType = CommandType.StoredProcedure;
                    if (dbObjects.Columns.Contains("specific_catalog") && dbObjects.Columns.Contains("specific_schema"))
                    {
                        DataTable procedureParameters = connection.GetSchema("ProcedureParameters");
                        DataRow[] theparameters =
                            procedureParameters.Select(
                                "specific_catalog = \'" + existingObjects[0]["specific_catalog"] + "\' AND specific_schema = \'" + existingObjects[0]["specific_schema"] +
                                "' AND specific_name = '" + existingObjects[0]["specific_name"] + "\'",
                                "ordinal_position ASC");
                        if (theparameters.Length > 0)
                        {
                            parameters = new List<DbParameter>(theparameters.Length);
                            foreach (DataRow paraminfo in theparameters)
                            {
                                DbParameter theParameter = _providerFactory.CreateParameter();
#if CLR_AT_LEAST_3_5 
                                    theParameter.ParameterName = paraminfo.Field<string>("parameter_name");
                                    theParameter.DbType = MapToDbType(paraminfo.Field<string>("data_type"));
#else
                                theParameter.ParameterName = paraminfo["parameter_name"] as string;
                                theParameter.DbType = MapToDbType(paraminfo["data_type"] as string);
#endif
                                parameters.Add(theParameter);
                            }
                        }
                    }
                }
            }
            return commandType;
        }
        /// <summary>
        /// Clear data adapters cached
        /// </summary>
        private void ClearDataAdaptersCached()
        {
            try
            {
                foreach (KeyValuePair<DbConnection, string> key in new List<KeyValuePair<DbConnection, string>>(_dataAdaptersCached.Keys))
                {
                    try
                    {
                        DbDataAdapter dbDataAdapter = _dataAdaptersCached[key];
                        if (dbDataAdapter != null) dbDataAdapter.Dispose();
                        _dataAdaptersCached[key] = null;
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                _dataAdaptersCached.Clear();
            }
        }
        /// <summary>
        /// Saves the information on the RecordsetHelper to a XML document.
        /// </summary>
        /// <param name="document">The XML document to save the data to.</param>
        public void Save(XmlDocument document)
        {
            using (StringWriter writer = new StringWriter())
            {
                WriteXml(writer, XmlWriteMode.WriteSchema);
                document.LoadXml(writer.ToString());
                writer.Close();
            }
        }

        /// <summary>
        /// clears the data adapter cache and set caching adapter to TRUE
        /// </summary>
        public void StartCachingAdapter()
        {
            ClearDataAdaptersCached();
            _cachingAdapter = true;
        }
        /// <summary>
        /// Stop caching the adapters used for connections
        /// </summary>
        public void StopCachingAdapter()
        {
            ClearDataAdaptersCached();
            _cachingAdapter = false;
        }

        /// <summary>
        /// Opens the connection and initialize the RecordsetHelper object.
        /// </summary>
        public override void Open()
        {
            try
            {
                Open(false);
            } // try
            catch
            {
#if WINFORMS
				if (Process.GetCurrentProcess().ProcessName == "devenv")
				{
					System.Windows.Forms.MessageBox.Show("There was a problem opening the recordset. Please verify connection string", TITLE_DIALOG_RecordSetError);
				}
#endif
            } // catch
        }

        /// <summary>
        /// Creates a DBCommand object using de provided parameters.
        /// </summary>
        /// <param name="commandText">A string containing the SQL query.</param>
        /// <param name="commandType">The desire type for the command.</param>
        /// <param name="parameters">A list with the parameters to be included on the DBCommand object.</param>
        /// <returns>A new DBCommand object.</returns>
        protected override DbCommand CreateCommand(String commandText, CommandType commandType, List<DbParameter> parameters)
        {
            Debug.Assert(_providerFactory != null, "Providerfactory must not be null");
            DbCommand command = _providerFactory.CreateCommand();
            switch (commandType)
            {
                case CommandType.StoredProcedure:
                    string[] commandParts = commandText.Split(' ', ',');
                    if (command != null)
                    {
                        command.CommandType = commandType;
                        command.CommandText = commandParts[0];
                        if (parameters != null && (parameters.Count == commandParts.Length - 1))
                        {
                            for (int i = 1; i < commandParts.Length; i++)
                            {
                                DbParameter parameter = parameters[i - 1];
                                object value = commandParts[i];
                                //conversions might be needed for various types
                                //currently there is only a convertion for Guid types. New convertions will be added as needed
                                if (parameter.DbType == DbType.Guid)
                                {
                                    //Remove single quotes if present to avoid exception in Guid constructor
                                    string strValue = commandParts[i].Replace("'", "");
                                    value = new Guid(strValue);
                                }
                                parameter.Value = value;
                            }
                            command.Parameters.AddRange(parameters.ToArray());
                        }
                    }
                    break;
                case CommandType.TableDirect:
                    //ODBC and SQL Client providers do not support table direct commands
                    string providerType = _providerFactory.GetType().ToString();
                    if (providerType.StartsWith("System.Data.Odbc") || providerType.StartsWith("System.Data.SqlClient"))
                    {
                        if (command != null)
                        {
                            command.CommandType = CommandType.Text;
                            command.CommandText = "Select * from " + commandText;
                        }
                    }
                    else
                    {
                        goto default;
                    }
                    break;
                default:
                    if (command != null)
                    {
                        command.CommandType = commandType;
                        command.CommandText = commandText;
                    }
                    break;
            }
            return command;
        }

        /// <summary>
        /// Opens the RecordsetHelper and requeries according to the value of �requery� parameter.
        /// </summary>
        /// <param name="requery">Indicates if a requery most be done.</param>
        public void Open(bool requery)
        {
            if (!requery)
            {
                Validate();
            }
            if (_activeCommand == null && (_source is String))
            {
                List<DbParameter> parameters;
                CommandType commandType = getCommandType((string)_source, out parameters);
                _activeCommand = CreateCommand((string)_source, commandType, parameters);
            }
            if (ActiveConnection == null && _activeCommand != null && _activeCommand.Connection != null)
            {
                ActiveConnection = _activeCommand.Connection;
            }

            OpenRecordset(requery);
        }

        /// <summary>
        /// Creates an update command using the information contained in the RecordsetHelper.
        /// </summary>
        /// <returns>A DBCommand object containing an update command.</returns>
        protected DbCommand CreateUpdateCommandFromMetaData()
        {
            int i = 0, j = 0;
            DbCommand result = null;
            String tableName = getTableName(_activeCommand.CommandText);
            try
            {
                if (!string.IsNullOrEmpty(tableName))
                {
                    string updatePart = "";
                    string wherePart = "";
                    string sql = "";
                    List<DbParameter> listGeneral = new List<DbParameter>();
                    List<DbParameter> listWhere = new List<DbParameter>();

                    foreach (System.Data.DataColumn dColumn in Tables[CurrentRecordSet].Columns)
                    {
                        if (Tables[CurrentRecordSet].PrimaryKey != null && !(Array.Exists(
                            Tables[CurrentRecordSet].PrimaryKey,
                            delegate(DataColumn col) { return col.ColumnName.Equals(dColumn.ColumnName, StringComparison.InvariantCultureIgnoreCase); })
                              || dColumn.ReadOnly))
                        {
                            if (updatePart.Length > 0)
                            {
                                updatePart += " , ";
                            }

                            updatePart += dColumn.ColumnName + " = ?";
                            listGeneral.Add(CreateParameterFromColumn("p" + (++i), dColumn));
                        }
                        if (wherePart.Length > 0)
                        {
                            wherePart += " AND ";
                        }
                        DbParameter param;
                        if (dColumn.AllowDBNull)
                        {
                            wherePart += "((? = 1 AND " + dColumn.ColumnName + " IS NULL) OR (" + dColumn.ColumnName + " = ?))";
                            param = CreateParameterFromColumn("q" + (++j), dColumn);
                            param.DbType = DbType.Int32;
                            param.SourceVersion = DataRowVersion.Original;
                            param.SourceColumnNullMapping = true;
                            param.Value = 1;
                            listWhere.Add(param);
                            param = CreateParameterFromColumn("q" + (++j), dColumn);
                            param.SourceVersion = DataRowVersion.Original;
                            listWhere.Add(param);
                        }
                        else
                        {
                            wherePart += "(" + dColumn.ColumnName + " = ?)";
                            param = CreateParameterFromColumn("q" + (++j), dColumn);
                            param.SourceVersion = DataRowVersion.Original;
                            listWhere.Add(param);
                        }
                    }
                    listGeneral.AddRange(listWhere);
                    sql = "UPDATE " + tableName + " SET " + updatePart + " WHERE " + wherePart;
                    result = ProviderFactory.CreateCommand();
                    if (result != null)
                    {
                        result.CommandText = sql;
                        listGeneral.ForEach(delegate(DbParameter p) { result.Parameters.Add(p); });
                        result.Connection = _activeCommand.Connection;
                    }
                }
            }
            catch
            {
            }
            return result;
        }
        /// <summary>
        /// Creates the update command for the database update operations of the recordset
        /// </summary>
        /// <param name="adapter">The data adapter that will contain the update command</param>
        /// <param name="cmdBuilder">The command builder to get the update command from.</param>
        protected virtual void CreateUpdateCommand(DbDataAdapter adapter, DbCommandBuilder cmdBuilder)
        {
            try
            {
                adapter.UpdateCommand = (string.IsNullOrEmpty(_sqlUpdateQuery)) ? cmdBuilder.GetUpdateCommand(true) : CreateCommand(_sqlUpdateQuery, CommandType.Text, null);
            }
            catch (Exception)
            {
                adapter.UpdateCommand = CreateUpdateCommandFromMetaData();
            }
        }

        /// <summary>
        /// Creates a delete command using the information contained in the RecordsetHelper.    
        /// </summary>
        /// <returns>A DBCommand object containing a delete command.</returns>
        protected DbCommand CreateDeleteCommandFromMetaData()
        {
            DbCommand result = null;
            String tableName = getTableName(_activeCommand.CommandText);
            int j = 0;
            try
            {
                if (!string.IsNullOrEmpty(tableName))
                {
                    string wherePart = "";
                    List<DbParameter> listGeneral = new List<DbParameter>();

                    foreach (DataColumn dColumn in Tables[CurrentRecordSet].Columns)
                    {
                        if (wherePart.Length > 0)
                        {
                            wherePart += " AND ";
                        }

                        DbParameter pInfo;
                        if (dColumn.AllowDBNull)
                        {
                            wherePart += "((? = 1 AND " + dColumn.ColumnName + " IS NULL) OR (" + dColumn.ColumnName + " = ?))";

                            pInfo = CreateParameterFromColumn("p" + (++j), dColumn);
                            pInfo.DbType = DbType.Int32;
                            pInfo.SourceVersion = DataRowVersion.Original;
                            pInfo.SourceColumnNullMapping = true;
                            pInfo.Value = 1;
                            listGeneral.Add(pInfo);

                            pInfo = CreateParameterFromColumn("p" + (++j), dColumn);
                            pInfo.SourceVersion = DataRowVersion.Original;
                            listGeneral.Add(pInfo);
                        }
                        else
                        {
                            wherePart += "(" + dColumn.ColumnName + " = ?)";
                            pInfo = CreateParameterFromColumn("q" + (++j), dColumn);
                            pInfo.SourceVersion = DataRowVersion.Original;
                            listGeneral.Add(pInfo);
                        }
                    }
                    string sql = "DELETE FROM " + tableName + " WHERE (" + wherePart + ")";
                    result = ProviderFactory.CreateCommand();
                    if (result != null)
                    {
                        result.CommandText = sql;
                        listGeneral.ForEach(delegate(DbParameter p) { result.Parameters.Add(p); });
                        result.Connection = _activeCommand.Connection;
                    }
                }
            }
            catch
            {
            }
            return result;
        }
        /// <summary>
        /// Creates an insert command using the information contained in the RecordsetHelper.
        /// </summary>
        /// <returns>A DBCommand object containing an insert command.</returns>
        protected DbCommand CreateInsertCommandFromMetaData()
        {
            DbCommand result = null;
            int i = 0;
            String tableName = getTableName(_activeCommand.CommandText);
            try
            {

                if (!string.IsNullOrEmpty(tableName))
                {
                    List<DbParameter> parameters = new List<DbParameter>();
                    string fieldsPart = "";
                    string valuesPart = "";
                    string sql;
                    foreach (DataColumn dColumn in Tables[CurrentRecordSet].Columns)
                    {
                        if (!dColumn.ReadOnly)
                        {
                            if (fieldsPart.Length > 0)
                            {
                                fieldsPart += ", ";
                                valuesPart += ", ";
                            }

                            fieldsPart += dColumn.ColumnName;
                            valuesPart += "?";
                            parameters.Add(CreateParameterFromColumn("p" + (++i), dColumn));
                        }
                    }
                    sql = "INSERT INTO " + tableName + " (" + fieldsPart + ") VALUES (" + valuesPart + ")";
                    result = ProviderFactory.CreateCommand();
                    if (result != null)
                    {
                        result.CommandText = sql;
                        parameters.ForEach(delegate(DbParameter p) { result.Parameters.Add(p); });
                        result.Connection = _activeCommand.Connection;
                    }
                }
            }
            catch
            {
            }
            return result;
        }



        /// <summary>
        /// SqlServer Identity value for last insert execution.
        /// </summary>
        /// <param name="adapter">DbDataAdapter to set</param>
        /// <param name="identityInfo">Name of Identity field</param>
        /// <param name="extraCommandText">used to set the query to get the identity value</param>
        /// <returns>returns the entire query in the adapter</returns>
        protected string MsInsertCommandCompletion(DbDataAdapter adapter, String identityInfo, string extraCommandText)
        {
            if (!String.IsNullOrEmpty(identityInfo))
            {
                DbParameter outPar = _providerFactory.CreateParameter();
                if (outPar != null)
                {
                    outPar.ParameterName = "@" + identityInfo;
                    outPar.DbType = GetDBType(Tables[CurrentRecordSet].Columns[identityInfo].DataType);
                    outPar.Direction = ParameterDirection.Output;
                    outPar.SourceColumn = identityInfo;
                }
                extraCommandText += " SELECT @" + identityInfo + " = SCOPE_IDENTITY()";
                adapter.InsertCommand.Parameters.Add(outPar);
            }
            return extraCommandText;
        }

        /// <summary>
        /// Assigns the InsertCommand to the adaptar parameter
        /// </summary>
        /// <param name="adapter">DbDataAdapter</param>
        protected void CompleteInsertCommand(DbDataAdapter adapter)
        {
            String extraCommandText = "";
            String extraCommandText1 = "";
            Dictionary<String, String> identities = IdentityColumnsManager.GetIndentityInformation(getTableName(_activeCommand.CommandText));
            if (identities != null)
            {
                foreach (KeyValuePair<String, String> identityInfo in identities)
                {
                    adapter.InsertCommand.UpdatedRowSource = UpdateRowSource.Both;
                    //outPar.ParameterName = (isOracle ? ":" : "@") + identityInfo.Key;

                    if (DatabaseType == DatabaseType.Oracle)
                    {
                        DbParameter outPar = adapter.InsertCommand.Parameters[":" + identityInfo.Key];
                        //todo: check for null
                        outPar.Direction = ParameterDirection.Output;
                        outPar.DbType = GetDBType(Tables[CurrentRecordSet].Columns[identityInfo.Key].DataType);

                        if (String.IsNullOrEmpty(extraCommandText))
                        {
                            extraCommandText = " RETURNING " + identityInfo.Key;
                            extraCommandText1 = " INTO :" + identityInfo.Key;
                        }
                        else
                        {
                            extraCommandText += ", " + identityInfo.Key;
                            extraCommandText1 += ", :" + identityInfo.Key;
                        }
                    }
                    else if (DatabaseType != DatabaseType.Undefined)
                    {
                        extraCommandText = MsInsertCommandCompletion(adapter, identityInfo.Key, extraCommandText);
                    }
                }
            }
            else
            {
                extraCommandText = MsInsertCommandCompletion(adapter, _autoIncrementCol, extraCommandText);
            }
            adapter.InsertCommand.CommandText += extraCommandText + extraCommandText1;
        }

        /// <summary>
        /// Oracle event for row update
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">OracleRowUpdated event args</param>
        protected void RecordSetHelper_RowUpdatedOracle(object sender, OracleRowUpdatedEventArgs e)
        {
            if (e.StatementType == StatementType.Insert)
            {
                Dictionary<String, String> identities = IdentityColumnsManager.GetIndentityInformation(getTableName(_activeCommand.CommandText));
                if (identities != null)
                {
                    DbCommand oCmd = _providerFactory.CreateCommand();
                    if (oCmd != null)
                    {
                        oCmd.Connection = ActiveConnection;
                        oCmd.Transaction = TransactionManager.GetTransaction(ActiveConnection);
                        foreach (KeyValuePair<String, String> identityInfo in identities)
                        {
                            oCmd.CommandText = "Select " + identityInfo.Value + ".Currval from dual";
                            e.Row[identityInfo.Key] = oCmd.ExecuteScalar();
                        }
                    }
                    e.Row.AcceptChanges();
                }
            }
        }

        /// <summary>
        /// OleDb Row Updated event
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Row updated event args</param>
        protected void RecordSetHelper_RowUpdatedOleDb(object sender, System.Data.OleDb.OleDbRowUpdatedEventArgs e)
        {
            //This behavior depends on the database we are interacting with
            if (e.StatementType == StatementType.Insert && e.Status == UpdateStatus.Continue)
            {
                Dictionary<String, String> identities = IdentityColumnsManager.GetIndentityInformation(getTableName(_activeCommand.CommandText));
                if (identities != null)
                {
                    DbCommand oCmd = e.Command.Connection.CreateCommand();
                    oCmd.Transaction = e.Command.Transaction;
                    foreach (KeyValuePair<String, String> identityInfo in identities)
                    {
                        switch (DatabaseType)
                        {
                            case DatabaseType.Oracle:
                                oCmd.CommandText = "Select " + identityInfo.Value + ".Currval from dual";
                                break;
                            case DatabaseType.SQLServer:
                                oCmd.CommandText = "SELECT SCOPE_IDENTITY()";
                                break;
                            case DatabaseType.Access:
                                oCmd.CommandText = "SELECT @@IDENTITY";
                                break;
                        }
                        e.Row[identityInfo.Key] = oCmd.ExecuteScalar();
                    }
                    e.Row.AcceptChanges();
                }
            }
        }

        /// <summary>
        /// Using connection parameter creates a Database Data Adapter
        /// </summary>
        /// <param name="connection">DbConnection parameter</param>
        /// <param name="updating">if updating creates all internal query strings</param>
        /// <returns></returns>
        protected DbDataAdapter CreateAdapter(DbConnection connection, bool updating)
        {
            Debug.Assert(connection != null, "Error during CreateAdapter call. Connection String must never be null");
            DbDataAdapter realAdapter = ProviderFactory.CreateDataAdapter();
            DbDataAdapter adapter = ProviderFactory.CreateDataAdapter();
            bool isOracleProvider = ProviderFactory.GetType().FullName.Equals("Oracle.DataAccess.Client.OracleClientFactory");
            DbCommandBuilder cmdBuilder = null;
            KeyValuePair<DbConnection, string> key = new KeyValuePair<DbConnection, string>();
            try
            {
                cmdBuilder = ProviderFactory.CreateCommandBuilder();

                if (_activeCommand.Connection == null || _activeCommand.Connection.ConnectionString.Equals(""))
                {
                    //What should we use here. ActiveConnection or the connection we are sending as parameter
                    //it seams more valid to use the parameter
                    _activeCommand.Connection = connection;
                }
                if (String.IsNullOrEmpty(_activeCommand.CommandText))
                {
                    _activeCommand.CommandText = _sqlSelectQuery;
                }
                DbTransaction transaction = TransactionManager.GetTransaction(connection);
                if (transaction != null)
                {
                    _activeCommand.Transaction = transaction;
                }

                if (_cachingAdapter)
                {
                    key = new KeyValuePair<DbConnection, string>(_activeCommand.Connection, _activeCommand.CommandText);
                    if (_dataAdaptersCached.ContainsKey(key))
                    {
                        return _dataAdaptersCached[key];
                    }
                }

                if (adapter != null)
                {
                    adapter.SelectCommand = _activeCommand;
                    realAdapter.SelectCommand = adapter.SelectCommand;
                    cmdBuilder.DataAdapter = adapter;
                    if (updating)
                    {
                        if (DatabaseType == DatabaseType.Access || DatabaseType == DatabaseType.SQLServer || getTableName(_activeCommand.CommandText).Contains(" "))
                        {
                            cmdBuilder.QuotePrefix = "[";
                            cmdBuilder.QuoteSuffix = "]";
                        }
                        CreateUpdateCommand(adapter, cmdBuilder);
                        try
                        {
                            adapter.InsertCommand = (string.IsNullOrEmpty(_sqlInsertQuery)) ? cmdBuilder.GetInsertCommand(true) : CreateCommand(_sqlInsertQuery, CommandType.Text, null);
                        }
                        catch (Exception)
                        {
                            adapter.InsertCommand = CreateInsertCommandFromMetaData();
                        }
                        try
                        {
                            adapter.DeleteCommand = (string.IsNullOrEmpty(_sqlDeleteQuery)) ? cmdBuilder.GetDeleteCommand(true) : CreateCommand(_sqlDeleteQuery, CommandType.Text, null);
                        }
                        catch (Exception)
                        {
                            adapter.DeleteCommand = CreateDeleteCommandFromMetaData();
                        }

#if SUPPORT_OBSOLETE_ORACLECLIENT                            
                        if ((ProviderFactory is System.Data.SqlClient.SqlClientFactory) || isOracleProvider)
#else
                        if ((ProviderFactory is System.Data.SqlClient.SqlClientFactory))
#endif
                        {
#if SUPPORT_OBSOLETE_ORACLECLIENT                            
			                //EVG20080326: Oracle.DataAccess Version 10.102.2.20 Bug. It returns "::" instead ":" before each parameter name, wich is invalid.
			                if (isOracleProvider)
			                {
			                    adapter.InsertCommand.CommandText = adapter.InsertCommand.CommandText.Replace("::", ":");
			                    adapter.DeleteCommand.CommandText = adapter.DeleteCommand.CommandText.Replace("::", ":");
			                    adapter.UpdateCommand.CommandText = adapter.UpdateCommand.CommandText.Replace("::", ":");
			                }
#endif
                            CompleteInsertCommand(adapter);
                        }
                        else if (ProviderFactory is System.Data.OleDb.OleDbFactory)
                        {
                            ((System.Data.OleDb.OleDbDataAdapter)realAdapter).RowUpdated += RecordSetHelper_RowUpdatedOleDb;
                        }
                        realAdapter.InsertCommand = CloneCommand(adapter.InsertCommand);
                        realAdapter.DeleteCommand = CloneCommand(adapter.DeleteCommand);
                        realAdapter.UpdateCommand = CloneCommand(adapter.UpdateCommand);
                        if (realAdapter.InsertCommand != null)
                        {
                            realAdapter.InsertCommand.Transaction = realAdapter.SelectCommand.Transaction;
                        }
                        if (realAdapter.DeleteCommand != null)
                        {
                            realAdapter.DeleteCommand.Transaction = realAdapter.SelectCommand.Transaction;
                        }
                        if (realAdapter.UpdateCommand != null)
                        {
                            realAdapter.UpdateCommand.Transaction = realAdapter.SelectCommand.Transaction;
                        }
                    }
                }

                if (_cachingAdapter)
                {
                    _dataAdaptersCached.Add(key, realAdapter);
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                if (adapter != null) adapter.Dispose();
                if (cmdBuilder != null)
                {
                    cmdBuilder.Dispose();
                }
            }
            return realAdapter;
        }

        /// <summary>
        /// Opens the RecordsetHelper and requeries according to the value of �requery� parameter.
        /// </summary>
        /// <param name="requery">Indicates if a requery most be done.</param>
        protected void OpenRecordset(bool requery)
        {
            _firstEof = true;
            if (ActiveConnection != null && _activeCommand != null)
            {
                DbDataAdapter dbAdapter = null;
                try
                {
                    dbAdapter = CreateAdapter(ActiveConnection, false);
                    _sqlSelectQuery = _activeCommand.CommandText;
                    OperationFinished = false;
                    this.Tables.Clear();
                    if (LoadSchema)
                    {
                        using (DataTable tmpTable = new DataTable())
                        {
                            if ((Tables.Count > 0) && (Tables[CurrentRecordSet].Columns.Count > 0))
                            {
                                dbAdapter.FillSchema(tmpTable, SchemaType.Source);
                            }
                        }
                    }
                    if (!LoadSchemaOnly)
                        dbAdapter.Fill(this);
                    else
                        dbAdapter.FillSchema(this, SchemaType.Source);
                }
                finally
                {
                    if (!IsCachingAdapter)
                        if (dbAdapter != null) dbAdapter.Dispose();
                }
            }
            if (Tables.Count > 0)
            {
                FixAutoincrementColumns(Tables[CurrentRecordSet]);
                OperationFinished = true;
                _currentView = Tables[CurrentRecordSet].DefaultView;
                _currentView.AllowDelete = true;
                _currentView.AllowEdit = true;
                _currentView.AllowNew = true;
                if (Tables[CurrentRecordSet].Rows.Count == 0)
                {
                    _index = -1;
                    _eof = true;
                }
                else
                {
                    _index = 0;
                    _eof = false;
                }
            }
            else
            {
                _index = -1;
                _eof = true;
            }
            _newRow = false;
            _opened = true;
            OnAfterQuery();
        }


        /// <summary>
        /// Opens the DAORecordsetHelper object by executing the query in the �command� parameter and load all results.
        /// </summary>
        /// <param name="command">A command containing the query to be execute to load the DAORecordsetHelper object.</param>
        public void Open(DbCommand command)
        {
            Validate();
            _source = command;
            _activeCommand = command;
            Open();
        }

        /// <summary>
        /// Opens the DAORecordsetHelper object by executing the query on the �SQLstr� using the connection object provided has parameter and load all results.
        /// </summary>
        /// <param name="sqLstr">The string containing the SQL query to be loaded into this DAORecodsetHelper object.</param>
        /// <param name="connection">Connection object to be use by this DAORecordsetHelper.</param>
        public void Open(String sqLstr, DbConnection connection)
        {
            ActiveConnection = connection;
            List<DbParameter> parameters;
            CommandType commandType = getCommandType(sqLstr, out parameters);
            Open(CreateCommand(sqLstr, commandType, parameters));
        }

        /// <summary>
        /// Creates a new DAORecordsetHelper object using the �factoryName� and opens it by executing the query on the �SQLstr� using the connection object provided has parameter and load all results.
        /// </summary>
        /// <param name="sqlStr">The string containing the SQL query to be loaded into this DAORecodsetHelper object.</param>
        /// <param name="connection">Connection object to be use by this DAORecordsetHelper.</param>
        /// <param name="factoryName">The name of the factory to by use by this DAORecordsetHelper object (the name most exist on the configuration xml file).</param>
        /// <returns></returns>
        public static DAORecordSetHelper Open(string sqlStr, DbConnection connection, string factoryName)
        {
            DAORecordSetHelper recordSet = new DAORecordSetHelper(factoryName);
            recordSet.Open(sqlStr, connection);
            return recordSet;
        }

        /// <summary>
        /// Creates a new DAORecordsetHelper object using the �factoryName�, �type� and opens it by executing the query on the �SQLstr� using the connection object provided has parameter and load all results.
        /// </summary>
        /// <param name="sqlStr">The string containing the SQL query to be loaded into this DAORecodsetHelper object.</param>
        /// <param name="type">The DAORecordsetTypeEnum of this DAORecordsetHelper object.</param>
        /// <param name="connection">Connection object to be use by this DAORecordsetHelper.</param>
        /// <param name="factoryName">The name of the factory to by use by this DAORecordsetHelper object (the name most exist on the configuration xml file).</param>
        /// <returns>The new DAORecordsetHelper object.</returns>
        public static DAORecordSetHelper Open(string sqlStr, DAORecordsetTypeEnum type, DbConnection connection, string factoryName)
        {
            // Type not used.
            return Open(sqlStr, connection, factoryName);
        }

        /// <summary>
        /// Creates a new DAORecordsetHelper object using the �factoryName�, �type�, "options" and opens it by executing the query on the �SQLstr� using the connection object provided has parameter and load all results.
        /// </summary>
        /// <param name="sqlStr">The string containing the SQL query to be loaded into this DAORecodsetHelper object.</param>
        /// <param name="type">The DAORecordsetTypeEnum of this DAORecordsetHelper object.</param>
        /// <param name="options">The DAORecordsetOptionEnum of this DAORecordsetHelper object.</param>
        /// <param name="connection">Connection object to be use by this DAORecordsetHelper.</param>
        /// <param name="factoryName">The name of the factory to by use by this DAORecordsetHelper object (the name most exist on the configuration xml file).</param>
        /// <returns>The new DAORecordsetHelper object.</returns>
        public static DAORecordSetHelper Open(string sqlStr, DAORecordsetTypeEnum type, DAORecordsetOptionEnum options, DbConnection connection, string factoryName)
        {
            // Type not used.
            // Options not used.
            return Open(sqlStr, connection, factoryName);
        }

        /// <summary>
        /// Creates a new DAORecordsetHelper object using the �factoryName�, �type�, "options", "lockType"  and opens it by executing the query on the �SQLstr� using the connection object provided has parameter and load all results.
        /// </summary>
        /// <param name="sqlStr">The string containing the SQL query to be loaded into this DAORecodsetHelper object.</param>
        /// <param name="type">The DAORecordsetTypeEnum of this DAORecordsetHelper object.</param>
        /// <param name="options">The DAORecordsetOptionEnum of this DAORecordsetHelper object.</param>
        /// <param name="lockType">The DAOLockTypeEnum of this DAORecordsetHelper object.</param>
        /// <param name="connection">Connection object to be use by this DAORecordsetHelper.</param>
        /// <param name="factoryName">The name of the factory to by use by this DAORecordsetHelper object (the name most exist on the configuration xml file).</param>
        /// <returns>The new DAORecordsetHelper object.</returns>
        public static DAORecordSetHelper Open(string sqlStr, DAORecordsetTypeEnum type, DAORecordsetOptionEnum options, DAOLockTypeEnum lockType, DbConnection connection, string factoryName)
        {
            DAORecordSetHelper recordSet = new DAORecordSetHelper(factoryName, type, options, lockType);
            recordSet.Open(sqlStr, connection);
            return recordSet;
        }

        /// <summary>
        /// Creates a new DAORecordsetHelper object using the �factoryName� and opens it by executing the query in the �command� parameter and load all results.
        /// </summary>
        /// <param name="command">A command containing the query to be execute to load the DAORecordsetHelper object.</param>
        /// <param name="factoryName">The name of the factory to by use by this DAORecordsetHelper object (the name most exist on the configuration xml file).</param>
        /// <returns>The new DAORecordsetHelper object.</returns>
        public static DAORecordSetHelper Open(DbCommand command, String factoryName)
        {
            DAORecordSetHelper recordSet = new DAORecordSetHelper(factoryName);
            recordSet.Open(command);
            return recordSet;
        }

        /// <summary>
        /// Save the current content of the DAORecordsetHelper to the database.
        /// </summary>
        /// <param name="UpdateType">The DAOUpdateTypeEnum to be use by this update.</param>
        /// <param name="Force">A Boolean value indicating whether or not to force the changes into the database.</param>
        public void Update(DAOUpdateTypeEnum UpdateType, bool Force)
        {
            //note: No case has been found to use the specialization parameters. 
            //if (UpdateType == DAOUpdateTypeEnum.dbUpdateRegular)
            Update();
        }



#if TargetF2 || TargetF35
        private object _updateLambda;

        /// <summary>
        /// Extension lambda for overriding default Update logic. This is helpful for
        /// Recordset loaded from Stored Procedures
        /// </summary>
        public object UpdateLambda
        {
            get { return _updateLambda; }
            set { _updateLambda = value; }
        }
        private void UpdateLambdaF2(DataRow theRow){}
#else
        /// <summary>
        /// Extension lambda for overriding default Update logic. This is helpful for
        /// Recordset loaded from Stored Procedures
        /// </summary>
        public Action<DAORecordSetHelper, DataRow> UpdateLambda { get; set; }
#endif


#if TargetF2 || TargetF35
        private Action<DAORecordSetHelper> _deleteLambda;

       /// <summary>
	   /// Extension lambda for overriding default Update logic. This is helpful for
	   /// Recordset loaded from Stored Procedures
	   /// </summary>
        public Action<DAORecordSetHelper> DeleteLambda
        {
            get { return _deleteLambda; }
            set { _deleteLambda = value; }
        }
#else
        /// <summary>
        /// Extension lambda for overriding default Update logic. This is helpful for
        /// Recordset loaded from Stored Procedures
        /// </summary>
        public Action<DAORecordSetHelper, DataRow> DeleteLambda { get; set; }
#endif
        /// <summary>
        /// List the posible update cases used with the UpdateInfo
        /// </summary>
        public enum UpdateType
        {
            /// <summary>
            /// Row Adde
            /// </summary>
            Added,
            /// <summary>
            /// Row deleted
            /// </summary>
            Deleted,
            /// <summary>
            /// Row Modified
            /// </summary>
            Modified
        }
        /// <summary>
        /// Event argument class for sending update information to listeners like DataControl helper classes
        /// </summary>
        public class UpdateInfo : EventArgs
        {
            /// <summary>
            /// The update type for the row
            /// </summary>
            public UpdateType UpdateType;
            /// <summary>
            /// Row Index
            /// </summary>
            public int index;
        }

        /// <summary>
        /// Event triggered before an update takes placed
        /// </summary>
        public event EventHandler BeforeUpdate;


        /// <summary>
        /// Event triggered after the updated has been completed
        /// </summary>
        public event EventHandler<UpdateInfo> AfterUpdate;


        /// <summary>
        /// Gets the current total number of records on the RecordsetHelper.
        /// </summary>
        public override int RecordCount
        {
            get
            {
                int count = 0;
                if (UsingView)
                {
                    count = _currentView.Count;
                }
                else if (Tables.Count > 0)
                {
                    count = Tables[CurrentRecordSet].Rows.Count;
                }
                if (_newRow && !UsingView)
                {
                    return count + 1;
                }
                return count;
            }
        }

        /// <summary>
        /// actual index
        /// </summary>
        protected int index = -1;

        private const string NotAllowedOperation = "Operation is not allowed when the object is closed.";

        /// <summary>
        /// 
        /// </summary>
        public override bool CanMovePrevious
        {
            get { return !BOF; }
        }

        /// <summary>
        /// Returns a value that indicates whether the current record position is before the first record in an ADORecordsetHelper object. Read-only Boolean.
        /// </summary>
        public override bool BOF
        {
            get
            {
                if (!Opened)
                {
                    throw new InvalidOperationException(NotAllowedOperation);
                }

                if (UsingView)
                    return index < 0 || _currentView.Count == 0;
                if (Tables.Count > 0)
                    return index < 0 || Tables[CurrentRecordSet].Rows.Count == 0;
                return true;
            }
        }


        private int _currentRecordSet = 0;
        /// <summary>
        /// Pointer to current recordset data table
        /// </summary>
        public override int CurrentRecordSet
        {
            get
            {
                return _currentRecordSet;
            }
            set
            {
                _currentRecordSet = value;
            }
        }

        /// <summary>
        /// Gets a DataRow with the information of the current record on the RecordsetHelper.
        /// </summary>
        public override DataRow CurrentRow
        {
            get
            {
                DataRow theRow = null;
                if (UsingView)
                {
                    _dbvRow = _currentView[_index];
                    theRow = _dbvRow.Row;
                }
                else
                {
                    if (_index < Tables[CurrentRecordSet].Rows.Count)
                    {
                        theRow = Tables[CurrentRecordSet].Rows[_index];
                    }
                }
                return theRow;
            }
        }

        /// <summary>
        /// Indicates if the ADORecordsetHelper is in batch mode.
        /// </summary>
        /// <returns>True if the ADORecordsetHelper is in batch mode, otherwise false.</returns>
        protected virtual bool isBatchEnabled()
        {
            return false;
        }

        /// <summary>
        /// Saves any changes made to the DataRow recieved as parameter.
        /// </summary>
        /// <param name="theRow">The row to be save on the Database.</param>
        protected void UpdateWithNoEvents(DataRow theRow)
        {
            OnBeforeUpdate();

#if TargetF2
			UpdateInfo info = null; 
#else
            UpdateInfo info = new UpdateInfo() { UpdateType = MatchRowState(theRow), index = this._index };
#endif

            if (UpdateLambda != null || DeleteLambda != null)
            {
                if (UpdateLambda != null && theRow.RowState == DataRowState.Modified)
                {
#if TargetF2 || TargetF35
                    UpdateLambdaF2(theRow);
#else
                    UpdateLambda(this, theRow);
#endif
                }
                else if (DeleteLambda != null && theRow.RowState == DataRowState.Deleted)
                {
#if TargetF2 || TargetF35
                    UpdateLambdaF2(theRow);
#else
                    DeleteLambda(this, theRow);
#endif
                }
                else
                {
#if TargetF2 || TargetF35
                    UpdateLambdaF2(theRow);
#else
                    UpdateLambda(this, theRow);
#endif
                }
            }
            else
            {
                //base.UpdateWithNoEvents(theRow);


                if (theRow.RowState != DataRowState.Unchanged)
                {
                    if (!isBatchEnabled())
                    {
                        DbConnection connection = GetConnection(ConnectionString);
                        DbDataAdapter dbAdapter = null;
                        try
                        {
                            dbAdapter = CreateAdapter(connection, true);
                            if (_requiresDefaultValues)
                                AssignDefaultValues(theRow);

                            dbAdapter.Update(new DataRow[] { theRow });
                        }
                        finally
                        {
                            if (!IsCachingAdapter)
                                if (dbAdapter != null) dbAdapter.Dispose();
                        }
                    }
                }

            }
            OnAfterUpdate(info);
        }

        private UpdateType MatchRowState(DataRow theRow)
        {
            switch (theRow.RowState)
            {
                case DataRowState.Added: return UpdateType.Added;

                case DataRowState.Deleted: return UpdateType.Deleted;
                case DataRowState.Modified: return UpdateType.Modified;
                default:
                    throw new NotImplementedException("Not ready for State" + theRow.RowState);
            }
        }

        private void OnAfterUpdate(UpdateInfo info)
        {
            if (AfterUpdate != null)
            {
                AfterUpdate(this, info);
            }
        }

        private void OnBeforeUpdate()
        {
            if (BeforeUpdate != null)
            {
                BeforeUpdate(this, new EventArgs());
            }
        }

        /// <summary>
        /// Sets the primary key to a DataTable object.
        /// </summary>
        /// <param name="dataTable">The DataTable that holds the currently loaded data.</param>
        private void FixAutoincrementColumns(DataTable dataTable)
        {
            if (ActiveConnection is System.Data.SqlClient.SqlConnection)
            {
                foreach (DataColumn col in dataTable.PrimaryKey)
                {
                    if (col.AutoIncrement)
                    {
                        col.AutoIncrementSeed = 0;
                        col.AutoIncrementStep = -1;
                        col.ReadOnly = false;
                        HasAutoincrementCols = true;
                        // todo check multiple autoincrement cases
                        _autoIncrementCol = col.ColumnName;
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Sets default values to a fields to avoid insert null in the DB when the field does not accept it.
        /// </summary>
        private void AssignDefaultValues(DataRow dbRow)
        {
            DbDataAdapter adapter = null;
            try
            {
                _requiresDefaultValues = false;
                if (_defaultValues == null) //no default values loaded for this table
                {
                    DataTable schemaTable;
                    try
                    {
                        adapter = CreateAdapter(GetConnection(ConnectionString), true);
                        schemaTable = ActiveConnection.GetSchema("Columns", new string[] { ActiveConnection.Database, "dbo", getTableName(adapter.SelectCommand.CommandText, true).Replace("dbo.", string.Empty) });
                    }
                    catch
                    {
                        return;
                    }

                    //Preloaded with the number  of elements required
                    _defaultValues = new List<KeyValuePair<bool, object>>();
                    for (int i = 0; i < Tables[CurrentRecordSet].Columns.Count; i++)
                    {
                        _defaultValues.Add(new KeyValuePair<bool, object>());
                    }
                    string defaultValue = String.Empty;
                    bool isComputed = false;
                    bool isUnknown = false;
                    for (int i = 0; i < schemaTable.Rows.Count; i++)
                    {
                        int thisColumnIndex = Tables[CurrentRecordSet].Columns.IndexOf(Convert.ToString(schemaTable.Rows[i]["COLUMN_NAME"]));
                        if (thisColumnIndex < 0) continue;

                        //13 Maximun length
                        if (Tables[CurrentRecordSet].Columns[thisColumnIndex].DataType == typeof(string))
                            Tables[CurrentRecordSet].Columns[thisColumnIndex].MaxLength = (schemaTable.Rows[i]["CHARACTER_MAXIMUM_LENGTH"] != DBNull.Value) ? Convert.ToInt32(schemaTable.Rows[i]["CHARACTER_MAXIMUM_LENGTH"]) : 255;

                        object originalValue = dbRow[thisColumnIndex];
                        if (schemaTable.Rows[i]["COLUMN_DEFAULT"] != DBNull.Value) //Has default value
                        {
                            defaultValue = (string)schemaTable.Rows[i]["COLUMN_DEFAULT"]; //8 Default Value
                            if (Tables[CurrentRecordSet].Columns[thisColumnIndex].DataType == typeof(bool))
                            {
                                dbRow[thisColumnIndex] = Convert.ToBoolean(Convert.ToDouble((defaultValue.Trim(new char[] { '(', ')', '\'' }))));
                            }
                            else
                            {
                                try
                                {
                                    string partialResult = (defaultValue.Trim(new char[] { '(', ')', '\'' }));
                                    if (Tables[CurrentRecordSet].Columns[thisColumnIndex].MaxLength != -1) //is string
                                        dbRow[thisColumnIndex] = partialResult.Length > Tables[CurrentRecordSet].Columns[thisColumnIndex].MaxLength ? partialResult.Substring(0, Tables[CurrentRecordSet].Columns[thisColumnIndex].MaxLength) : partialResult;
                                    else
                                        dbRow[thisColumnIndex] = partialResult;
                                }
                                catch
                                {
                                    try
                                    {
                                        dbRow[thisColumnIndex] = ComputeValue(defaultValue);
                                        isComputed = true;
                                    }
                                    catch
                                    {
                                        isUnknown = true;
                                    }
                                }
                            }
                        }
                        else
                        {
                            object isNullable = schemaTable.Rows[i]["IS_NULLABLE"];
                            bool tmpRes;
                            if (isNullable != null
                                && (string.Equals(Convert.ToString(isNullable), "No", StringComparison.InvariantCultureIgnoreCase)
                                || (bool.TryParse(Convert.ToString(isNullable), out tmpRes) && !tmpRes))) //Not Allow Null and has not default value
                            {
                                //Add more if necesary
                                if (Tables[CurrentRecordSet].Columns[thisColumnIndex].DataType == typeof(string))
                                    dbRow[thisColumnIndex] = string.Empty;
                                else if (Tables[CurrentRecordSet].Columns[thisColumnIndex].DataType == typeof(Int16))
                                    dbRow[thisColumnIndex] = default(Int16);
                                else if (Tables[CurrentRecordSet].Columns[thisColumnIndex].DataType == typeof(Int32))
                                    dbRow[thisColumnIndex] = default(Int32);
                                else if (Tables[CurrentRecordSet].Columns[thisColumnIndex].DataType == typeof(bool))
                                    dbRow[thisColumnIndex] = default(bool);
                                else if (Tables[CurrentRecordSet].Columns[thisColumnIndex].DataType == typeof(decimal))
                                    dbRow[thisColumnIndex] = default(decimal);
                                else if (Tables[CurrentRecordSet].Columns[thisColumnIndex].DataType == typeof(byte))
                                    dbRow[thisColumnIndex] = default(byte);
                                else if (Tables[CurrentRecordSet].Columns[thisColumnIndex].DataType == typeof(char))
                                    dbRow[thisColumnIndex] = default(char);
                            }
                            else
                                dbRow[thisColumnIndex] = DBNull.Value;
                        }
                        if (isComputed)
                        {
                            _defaultValues[thisColumnIndex] = new KeyValuePair<bool, object>(true, defaultValue);
                            isComputed = false;
                        }
                        else if (isUnknown)
                        {
                            _defaultValues[thisColumnIndex] = new KeyValuePair<bool, object>(false, DBNull.Value);
                            isUnknown = false;
                        }
                        else
                            _defaultValues[thisColumnIndex] = new KeyValuePair<bool, object>(false, dbRow[thisColumnIndex]);

                        if (originalValue != DBNull.Value)
                            dbRow[thisColumnIndex] = originalValue;
                    }
                }
                else //already _defaultValues has been created
                {
                    try
                    {
                        dbRow.BeginEdit();
                        for (int i = 0; i < _defaultValues.Count; i++)
                        {
                            if (dbRow[i] == DBNull.Value)
                            {
                                if (!_defaultValues[i].Key)
                                    dbRow[i] = _defaultValues[i].Value;
                                else
                                    dbRow[i] = ComputeValue((string)_defaultValues[i].Value);
                            }
                        }
                    }
                    finally
                    {
                        dbRow.EndEdit();
                    }
                }
            }
            finally
            {
                if (!IsCachingAdapter && adapter != null)
                    adapter.Dispose();
            }
        }

        private bool CheckNullState(DataRow row)
        {
            bool empty = true;
            foreach (object col in row.ItemArray)
            {
                if (col != null && !Convert.IsDBNull(col) && !String.IsNullOrEmpty(Convert.ToString(col)))
                {
                    empty = false;
                    break;
                }
            }
            if (empty)
            {
                _index = (_index > 0 ? _index-- : _index);
            }
            return empty;
        }
        /// <summary>
        /// Saves the changes done to the current record on the recordset.
        /// </summary>
        /// <remarks>If the recordset is not batch enabled this method saves the changes on the database.</remarks>
        public override void Update()
        {
            DataRow theRow = CurrentRow;
            if (theRow == null)
            {
                return;
            }
            if (_newRow)
            {
                _newRow = false;
            }
            int Action = -1;
            int Save = 0;

            if (theRow.RowState != DataRowState.Unchanged)
            {
                if (theRow.RowState == DataRowState.Added)
                {
                    Save = -1;
                    _editMode = EditModeEnum.EditAdd;
                    //AIS-TODO: Check if data row is empty do not call database operation
                    if (CheckNullState(theRow))
                    {
                        return;
                    }
                }
                OnValidating(ref Save, ref Action);
                if (Action == 0)
                {
                    _editMode = EditModeEnum.EditNone;
                    return;
                }
                if (!isBatchEnabled())
                {
                    UpdateWithNoEvents(theRow);
                }
            }
            _editMode = EditModeEnum.EditNone;
        }

        /// <summary>
        /// Moves the current index to the desire position provided has parameter.
        /// </summary>
        /// <param name="newIndex">The new index for the DAORecordsetHelper object.</param>
        protected void BasicMove(int newIndex)
        {
            _index = newIndex < 0 ? 0 : newIndex;
            _eof = _index >= (UsingView ? _currentView.Count : Tables[CurrentRecordSet].Rows.Count);
            //base.BasicMove(newIndex);
            OnAfterMove();
        }

        /// <summary>
        /// Moves the current location to the last position in the DAORecordset.
        /// </summary>
        public override void MoveLast()
        {
            BasicMove((UsingView ? _currentView.Count - 1 : Tables[CurrentRecordSet].Rows.Count - 1));
        }


        /// <summary>
        /// Closes an open object and any dependent objects.
        /// </summary>
        public override void Close()
        {
            try
            {
                if (Tables.Count > 0)
                {
                    Tables[CurrentRecordSet].Rows.Clear();
                }
                if (_activeCommand != null)
                {
                    _activeCommand.Connection = null;
                    _activeCommand.Dispose();
                }
                if (ActiveConnection != null)
                {
                    if (ActiveConnection.State == ConnectionState.Open && _connectionStateAtEntry == ConnectionState.Closed)
                    {
                        ActiveConnection.Close();
                    }
                }
                _opened = false;
                Dispose();
            }
            catch (Exception)
            {
            }
        }
        /// <summary>
        /// Moves the current record to the beginning of the RecordsetHelper.
        /// </summary>
        public override void MoveFirst()
        {
            BasicMove(0);
        }

        /// <summary>
        /// Moves the current record forward one position.
        /// </summary>
        public override void MoveNext()
        {
            BasicMove(_index + 1);
        }

        /// <summary>
        /// Moves the current record backwards one position.
        /// </summary>
        public override void MovePrevious()
        {
            BasicMove(_index - 1);
        }

        /// <summary>
        /// Creates and open a new DAORecordsetHelper using the same information of the current object.
        /// </summary>
        /// <returns>A new DAORecordset.</returns>
        public DAORecordSetHelper OpenRS()
        {
            DAORecordSetHelper newRecordSet = new DAORecordSetHelper();
            newRecordSet._activeCommand = _activeCommand;
            newRecordSet.ActiveConnection = ActiveConnection;
            newRecordSet.DatabaseType = DatabaseType;
            newRecordSet.ProviderFactory = ProviderFactory;
            newRecordSet.Open();
            return newRecordSet;
        }

        /// <summary>
        /// Creates and open a new DAORecordsetHelper using the same information of the current object and the type provided has parameter.
        /// </summary>
        /// <param name="rsType">The DAORecordsetTypeEnum of this DAORecordsetHelper object.</param>
        /// <returns>A new DAORecordset.</returns>
        public DAORecordSetHelper OpenRS(DAORecordsetTypeEnum rsType)
        {
            DAORecordSetHelper newRecordSet = new DAORecordSetHelper();
            newRecordSet._daoRsType = rsType;
            newRecordSet._activeCommand = _activeCommand;
            newRecordSet.ActiveConnection = ActiveConnection;
            newRecordSet.DatabaseType = DatabaseType;
            newRecordSet.ProviderFactory = ProviderFactory;
            newRecordSet.Open();
            return newRecordSet;
        }

        /// <summary>
        /// Creates and open a new DAORecordsetHelper using the same information of the current object and the type provided has parameter.
        /// </summary>
        /// <param name="rsType">The DAORecordsetTypeEnum of this DAORecordsetHelper object.</param>
        /// <param name="rsOptions">The DAORecordsetOptionEnum of this DAORecordsetHelper object.</param>
        /// <returns>A new DAORecordset.</returns>
        public DAORecordSetHelper OpenRS(DAORecordsetTypeEnum rsType, DAORecordsetOptionEnum rsOptions)
        {
            DAORecordSetHelper newRecordSet = new DAORecordSetHelper();
            newRecordSet._daoRsType = rsType;
            newRecordSet._daoRsOption = rsOptions;
            newRecordSet._activeCommand = _activeCommand;
            newRecordSet.ActiveConnection = ActiveConnection;
            newRecordSet.DatabaseType = DatabaseType;
            newRecordSet.ProviderFactory = ProviderFactory;
            newRecordSet.Open();
            return newRecordSet;
        }

        /// <summary>
        /// Updates the data in a Recordset object by re-executing the query on which the object is based.
        /// </summary>
        public override void Requery()
        {
            if (Tables.Count > 0)
            {
                Tables[CurrentRecordSet].Rows.Clear();
            }
            //base.Requery();
            Open(true);
        }

        /// <summary>
        /// Deletes the current record.
        /// </summary>
        public void Delete()
        {

            try
            {
                disableEventsWhileDeleting = false;
                //base.Delete();
                CurrentRow.Delete();
            }
            finally
            {
                disableEventsWhileDeleting = false;
            }
            Update();
        }
        #endregion

        /// <summary>
        /// Event Validating
        /// </summary>
        public event ValidatingEventHandler Validating;

        /// <summary>
        /// Fires DAORecordSetHelper Validating event that is listened by DAODataControlHelper
        /// </summary>
        /// <param name="Action"></param>
        /// <param name="Save"></param>
        protected virtual void OnValidating(ref int Action, ref int Save)
        {
            if (Validating != null)
            {
                ValidatingEventArgs vArgs = new ValidatingEventArgs(Action, Save);
                Validating(this, vArgs);
                Action = vArgs.Action;
                Save = vArgs.Save;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public DataRow LastModified
        {
            get
            {
                DataTable table = Tables[CurrentRecordSet];
                if (table.Rows.Count > 0)
                {
                    return table.Rows[table.Rows.Count - 1];
                }
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int EditMode
        {
            get
            {
                return (int)_editMode;
            }
        }

        /// <summary>
        /// is filtered?
        /// </summary>
        public bool Filtered
        {
            get { return _filtered; }
            set { _filtered = value; }
        }

        /// <summary>
        /// has auto increment columns
        /// </summary>
        public bool HasAutoincrementCols
        {
            get { return _hasAutoincrementCols; }
            set { _hasAutoincrementCols = value; }
        }

        /// <summary>
        /// has auto increment columns
        /// </summary>
        public bool IsDefaultSerializationInProgress
        {
            get { return _isDefaultSerializationInProgress; }
            set { _isDefaultSerializationInProgress = value; }
        }

        /// <summary>
        /// is deserilized
        /// </summary>
        public bool IsDeserialized
        {
            get { return _isDeserialized; }
            set { _isDeserialized = value; }
        }

        /// <summary>
        /// operation finished state
        /// </summary>
        public bool OperationFinished
        {
            get { return _operationFinished; }
            set { _operationFinished = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Refresh()
        {
            Requery();
        }
    }
}

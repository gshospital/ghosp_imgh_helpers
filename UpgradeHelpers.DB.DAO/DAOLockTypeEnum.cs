namespace UpgradeHelpers.DB.DAO
{
    /// <summary>Sets or returns the type of locking (concurrency) to use.</summary>
    public enum DAOLockTypeEnum
    {
        /// <summary>Optimistic concurrency based on record ID. Cursor compares record ID in old and new records to determine if changes have been made since the record was last accessed.</summary>
        dbOptimistic = 3,
        /// <summary>Enables batch optimistic updates (ODBCDirect workspaces only).</summary>
        dbOptimisticBatch = 5,
        /// <summary>Optimistic concurrency based on record values. Cursor compares data values in old and new records to determine if changes have been made since the record was last accessed (ODBCDirect workspaces only).</summary>
        dbOptimisticValue = 1,
        /// <summary>Pessimistic concurrency. Cursor uses the lowest level of locking sufficient to ensure that the record can be updated.</summary>
        dbPessimistic = 2
    }
}
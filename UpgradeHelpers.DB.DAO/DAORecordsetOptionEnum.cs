namespace UpgradeHelpers.DB.DAO
{
    /// <summary>
    /// Determines the way a recordset will be accessed setting some restriction or permitions.
    /// It will be used on OpenRecordset, and Execute methods.
    /// </summary>
    public enum DAORecordsetOptionEnum
    {
        /// <summary>Allows user to add new records to the dynaset, but prevents user from reading existing records.</summary>
        dbAppendOnly = 8,
        /// <summary>Applies updates only to those fields that will not affect other records in the dynaset (dynaset- and snapshot-type only).</summary>
        dbConsistent = 32,
        /// <summary>Prevents other users from reading Recordset records (table-type only).</summary>
        dbDenyRead = 2,
        /// <summary>Prevents other users from changing Recordset records.</summary>
        dbDenyWrite = 1,
        /// <summary>Executes the query without first calling the SQLPrepare ODBC function.</summary>
        dbExecDirect = 2048,
        /// <summary>Rolls back updates if an error occurs.</summary>
        dbFailOnError = 128,
        /// <summary>Creates a forward-only scrolling snapshot-type Recordset (snapshot-type only).</summary>
        dbForwardOnly = 256,
        /// <summary>Applies updates to all dynaset fields, even if other records are affected (dynaset- and snapshot-type only).</summary>
        dbInconsistent = 16,
        /// <summary>Opens the Recordset as read-only.</summary>
        dbReadOnly = 4,
        /// <summary>Executes the query asynchronously.</summary>
        dbRunAsync = 1024,
        /// <summary>Generates a run-time error if another user is changing data you are editing (dynaset-type only).</summary>
        dbSeeChanges = 512,
        /// <summary>Sends an SQL statement to an ODBC database (snapshot-type only).</summary>
        dbSQLPassThrough = 64
    }
}
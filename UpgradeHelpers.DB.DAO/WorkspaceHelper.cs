using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using UpgradeHelpers.DB.ADO;


namespace UpgradeHelpers.DB.DAO
{
    /// <summary>
    /// User Class
    /// </summary>
    public class User
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public User(string name)
        {
            Name = name;
        }
        /// <summary>
        /// 
        /// </summary>
        public void Refresh()
        {
            throw new NotImplementedException();
        }

        private Groups _groups;

        ///
        /// <summary>
        /// 
        /// </summary>
        public Groups Groups
        {
            get { return _groups; }
            set { _groups = value; }
        }

        private string _name;

        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
    }

    /// <summary>
    /// Group Class
    /// </summary>
    public class Group
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public Group(string name)
        {
            Name = name;
        }
        /// <summary>
        /// 
        /// </summary>
        public Users Users
        {
            get { return null; }
        }

        private string _name;

        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
    }

    /// <summary>
    /// Groups Class
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2237:MarkISerializableTypesWithSerializable")]
    public class Groups : Dictionary<string, Group>, IEnumerable<Group>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public new IEnumerator<Group> GetEnumerator()
        {
            return Values.GetEnumerator();
        }

        /// <summary>
        /// 
        /// </summary>
        public void Refresh()
        {
            if (this != null)
            {
                Clear();
            }
        }
    }

    /// <summary>
    /// Users Class
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2237:MarkISerializableTypesWithSerializable")]
    public class Users : Dictionary<string, User>, IEnumerable<User>
    {
        /// <summary>
        /// 
        /// </summary>
        public Users()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public new IEnumerator<User> GetEnumerator()
        {
            return Values.GetEnumerator();
        }

        /// <summary>
        /// 
        /// </summary>
        public void Refresh()
        {
            if (this != null)
            {
                Clear();
            }
        }

    }

    /// <summary>
    /// WorkspaceHelper Class
    /// A Workspace object defines a named session for a user. It contains open databases and provides mechanisms for simultaneous transactions.
    /// </summary>
    public class WorkspaceHelper : ConnectionContainers<DAODatabaseHelper>, IConnectionContainers
    {
        private readonly string user;
        private readonly string password;


        /// <summary>
        /// Begins a transaction for a specific connection.
        /// </summary>
        /// <param name="connection">The connection where the transaction will be initiated.</param>
        private void BeginTransaction(DAODatabaseHelper connection)
        {
            TransactionManager.Enlist(connection.Connection);
        }

        /// <summary>
        /// Closes a transaction for a specific connection.
        /// </summary>
        /// <param name="connection">The connection where the transaction will be close.</param>
        private void Close(DAODatabaseHelper connection)
        {
            TransactionManager.DeEnlist(connection.Connection);
            connection.Close();
        }

        /// <summary>
        /// Commits a transaction for a specific connection.
        /// </summary>
        /// <param name="connection">The connection where the transaction will be committed.</param>
        private void CommitTransaction(DAODatabaseHelper connection)
        {
            TransactionManager.Commit(connection.Connection);
        }

        /// <summary>
        /// Rollbacks a transaction for a specific connection.
        /// </summary>
        /// <param name="connection">The connection to work on.</param>
        private void Rollback(DAODatabaseHelper connection)
        {
            TransactionManager.Rollback(connection.Connection);
        }

        /// <summary>
        /// Event that notifies the current state of a change.
        /// </summary>
        /// <param name="sender">The object where the event was raised.</param>
        /// <param name="e">Additional event information.</param>
        void result_StateChange(object sender, System.Data.StateChangeEventArgs e)
        {
            if (e.CurrentState == System.Data.ConnectionState.Closed)
            {
                foreach (DAODatabaseHelper db in this.Connections)
                {
                    if (db.Connection == sender)
                    {
                        this.Connections.Remove(db);
                        break;
                    }
                }
            }
        }

        private readonly string name;

        #region Constructors

        /// <summary>
        /// Creates a new WorkspaceHelper object with the default configuration.
        /// </summary>
        public WorkspaceHelper()
        {
            name = "default";
        }

        /// <summary>
        /// Creates a new WorkspaceHelper object with the default configuration and the provided name.
        /// </summary>
        /// <param name="name">The name for the new WorkspaceHelper</param>
        public WorkspaceHelper(string name)
        {
            this.name = name;
        }

        /// <summary>
        /// Creates a new WorkspaceHelper object using the provided factory.
        /// </summary>
        /// <param name="factory">The factory to by use by this DAORecordsetHelper object (the name most exist on the configuration xml file).</param>
        public WorkspaceHelper(DbProviderFactory factory)
        {
            this.Factory = factory;
            name = "default";
        }

        /// <summary>
        /// Creates a new WorkspaceHelper object using the provided name and factory.
        /// </summary>
        /// <param name="name">The name for the new WorkspaceHelpe.r</param>
        /// <param name="factory">The factory to by use by this DAORecordsetHelper object (the name most exist on the configuration xml file).</param>
        public WorkspaceHelper(string name, DbProviderFactory factory)
        {
            this.Factory = factory;
            this.name = name;
        }
        /// <summary>
        /// Creates a new WorkspaceHelper object using the provided name and factory.
        /// </summary>
        /// <param name="name">The name for the new WorkspaceHelpe.r</param>
        /// <param name="factory">The factory to by use by this DAORecordsetHelper object (the name most exist on the configuration xml file).</param>
        /// <param name="user">The name for the new WorkspaceHelpe.r</param>
        /// <param name="password">The name for the new WorkspaceHelpe.r</param>
        public WorkspaceHelper(string name, DbProviderFactory factory, string user, string password)
        {
            this.Factory = factory;
            this.name = name;
            this.user = user;
            this.password = password;
            using (DbConnection cn = factory.CreateConnection())
            {
                if (cn != null)
                {
                    cn.ConnectionString = ConnectionString;
                    cn.Open();
                }
            }
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets the name that uniquely identifies this WorkspaceHelper.
        /// </summary>
        public string Name
        {
            get
            {
                return name;
            }
        }

        #endregion

        /// <summary>
        /// Opens a Database using the provided connection string.
        /// </summary>
        /// <param name="connectionString"></param>
        /// <returns>A DAODatabaseHelper object with the representation of the openned database.</returns>
        public DAODatabaseHelper OpenDatabase(String connectionString)
        {
            return (DAODatabaseHelper)Open(connectionString);
        }

        /// <summary>
        /// 
        /// </summary>
        private Users _users = null;

        /// <summary>
        /// 
        /// </summary>
        public Users Users
        {
            get
            {
                if (_users == null || _users.Count == 0)
                {
                    _users = new Users();
                    using (DbConnection cn = Factory.CreateConnection())
                    {
                        cn.ConnectionString = ConnectionString;
                        cn.Open();

                        using (DbCommand cmd = Factory.CreateCommand())
                        {
                            if (cmd != null)
                            {
                                cmd.Connection = cn;
                                cmd.CommandText = "select * from dbo.sysusers where islogin = 1";
                                using (DbDataReader reader = cmd.ExecuteReader())
                                {
                                    while (reader.Read())
                                    {
                                        string user = Convert.ToString(reader["name"]);
                                        _users.Add(user, new User(user));
                                    }
                                }
                            }
                        }

                        foreach (User user in _users)
                        {
                            Groups groups = new Groups();
                            using (DbCommand cmd = Factory.CreateCommand())
                            {
                                StringBuilder query = new StringBuilder("Select dp2.name ");
                                query.Append("from sys.database_principals dp2 ");
                                query.Append("where dp2.principal_id in ");
                                query.Append("( Select	drm.role_principal_id ");
                                query.Append("From	sys.database_principals dp, ");
                                query.Append("sys.database_role_members drm ");
                                query.Append("  Where ");
                                query.Append("dp.principal_id = drm.member_principal_id and ");
                                query.Append("dp.name = '{0}' ");
                                query.Append(") ");
                                query.Append("and dp2.type = 'R' and dp2.is_fixed_role = 0");
                                if (cmd != null)
                                {
                                    cmd.Connection = cn;
                                    cmd.CommandText = String.Format(query.ToString(), user.Name);
                                    using (DbDataReader reader = cmd.ExecuteReader())
                                    {
                                        while (reader.Read())
                                        {
                                            string name = Convert.ToString(reader["name"]);
                                            groups.Add(name, new Group(name));
                                        }
                                    }
                                }
                            }
                            user.Groups = groups;
                        }

                    }
                }
                return _users;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private Groups _groups = null;

        ///
        /// <summary>
        /// 
        /// </summary>
        public Groups Groups
        {
            get
            {
                if (_groups == null || _groups.Count == 0)
                {
                    _groups = new Groups();
                    using (DbConnection cn = Factory.CreateConnection())
                    {
                        if (cn != null)
                        {
                            cn.ConnectionString = ConnectionString;
                            cn.Open();

                            using (DbCommand cmd = Factory.CreateCommand())
                            {
                                cmd.Connection = cn;
                                cmd.CommandText = "select * from sys.database_principals where type = 'R' and is_fixed_role = 0";
                                using (DbDataReader reader = cmd.ExecuteReader())
                                {
                                    while (reader.Read())
                                    {
                                        string name = Convert.ToString(reader["name"]);
                                        _groups.Add(name, new Group(name));
                                    }
                                }
                            }
                        }
                    }
                }
                return _groups;
            }
            set { }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="databaseHelper"></param>
        /// <param name="Username"></param>
        /// <param name="Password"></param>
        /// <param name="UserRole"></param>
        public void CreateUser(DAODatabaseHelper databaseHelper, string Username, string Password, string UserRole)
        {
            BeginTransaction();
            DbCommand TempCommand_2 = databaseHelper.CreateCommand();
            TempCommand_2.CommandText = "sp_addnewuser";
            TempCommand_2.CommandType = System.Data.CommandType.StoredProcedure;
            TempCommand_2.Transaction = TransactionManager.GetTransaction(databaseHelper.Connection);
            DbCommand command = TempCommand_2;
            DAORecordSetHelper.commandParameterBinding(command, "Username").Value = Username;
            DAORecordSetHelper.commandParameterBinding(command, "Password").Value = Password;
            DAORecordSetHelper.commandParameterBinding(command, "UserRole").Value = UserRole;
            command.ExecuteNonQuery();
            CommitTransaction();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="databaseHelper"></param>
        /// <param name="Username"></param>
        public void DeleteUser(DAODatabaseHelper databaseHelper, string Username)
        {
            System.Data.Common.DbCommand TempCommand_2 = databaseHelper.CreateCommand();
            TempCommand_2.CommandText = String.Format("DROP USER {0}", Username);
            TempCommand_2.CommandType = System.Data.CommandType.Text;
            TempCommand_2.Transaction = TransactionManager.GetTransaction(databaseHelper.Connection);
            DbCommand command = TempCommand_2;
            command.ExecuteNonQuery();
            TempCommand_2 = databaseHelper.CreateCommand();
            TempCommand_2.CommandText = String.Format("DROP Login {0}", Username);
            command = TempCommand_2;
            command.ExecuteNonQuery();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="databaseHelper"></param>
        /// <param name="OldGroup"></param>
        /// <param name="NewGroup"></param>
        /// <param name="user"></param>
        public void UpdateRole(DAODatabaseHelper databaseHelper, string OldGroup, string NewGroup, string user)
        {
            System.Data.Common.DbCommand TempCommand_2 = databaseHelper.CreateCommand();
            TempCommand_2.CommandText = String.Format("exec sp_droprolemember '{0}', '{1}'", OldGroup, user);
            TempCommand_2.CommandType = System.Data.CommandType.Text;
            TempCommand_2.Transaction = TransactionManager.GetTransaction(databaseHelper.Connection);
            DbCommand command = TempCommand_2;
            command.ExecuteNonQuery();
            TempCommand_2 = databaseHelper.CreateCommand();
            TempCommand_2.CommandText = String.Format("exec sp_addrolemember '{0}', '{1}'", NewGroup, user);
            command = TempCommand_2;
            command.ExecuteNonQuery();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="databaseHelper"></param>
        /// <param name="user"></param>
        /// <param name="oldPassword"></param>
        /// <param name="newPassword"></param>
        public void NewPassword(DAODatabaseHelper databaseHelper, string user, string oldPassword, string newPassword)
        {
            BeginTransaction();
            DbCommand TempCommand_2 = databaseHelper.CreateCommand();
            TempCommand_2.CommandText = String.Format("ALTER LOGIN [{0}] WITH PASSWORD=N'{1}'", user, newPassword);
            TempCommand_2.CommandType = System.Data.CommandType.Text;
            TempCommand_2.Transaction = TransactionManager.GetTransaction(databaseHelper.Connection);
            DbCommand command = TempCommand_2;
            command.ExecuteNonQuery();
            CommitTransaction();
        }

        private string Credentials
        {
            get
            {
                if (String.IsNullOrEmpty(user) && String.IsNullOrEmpty(password))
                {
                    return String.Empty;
                }
                if (Factory is System.Data.Odbc.OdbcFactory)
                {
                    return String.Format(";UID='{0}';PWD={1};", user, password);
                }
                if (Factory is System.Data.SqlClient.SqlClientFactory)
                {
                    return String.Format(";User ID='{0}';Password={1};", user, password);
                }
                return string.Empty;
            }
        }
        private string ConnectionString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["QCCS.Properties.Settings.ConnectionString"].ConnectionString + Credentials;
            }
        }
        private string ConnectionStringSecurity
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["QCCS.Properties.Settings.SecurityConnectionString"].ConnectionString;
            }
        }
    }
}

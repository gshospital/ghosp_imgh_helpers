namespace UpgradeHelpers.DB.DAO
{
    /// <summary>Sets or returns the type of update to use.</summary>
    public enum DAOUpdateTypeEnum
    {
        /// <summary>All pending changes in the update cache are written to disk.</summary>
        dbUpdateBatch = 4,
        /// <summary>Only the current record's pending changes are written to disk.</summary>
        dbUpdateCurrentRecord = 2,
        /// <summary>(Default) Pending changes are not cached and are written to disk immediately.</summary>
        dbUpdateRegular = 1
    }
}
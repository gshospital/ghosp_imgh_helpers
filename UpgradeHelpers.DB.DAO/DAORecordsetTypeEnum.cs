namespace UpgradeHelpers.DB.DAO
{
    /// <summary>
    /// Determines the type of the recordset.
    /// It will be used on OpenRecordset method.
    /// </summary>
    public enum DAORecordsetTypeEnum
    {
        /// <summary>
        /// Open table
        /// </summary>
        dbOpenTable = 1,
        /// <summary>
        /// Dynamic
        /// </summary>
        dbOpenDynamic = 16,
        /// <summary>
        /// Dynaset
        /// </summary>
        dbOpenDynaset = 2,
        /// <summary>
        /// Snapshot
        /// </summary>
        dbOpenSnapshot = 4,
        /// <summary>
        /// Forward Only
        /// </summary>
        dbOpenForwardOnly = 8
    }
}
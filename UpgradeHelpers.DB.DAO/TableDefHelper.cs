using System;
using System.Data;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace UpgradeHelpers.DB.DAO
{

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class TableDefHelper : DataTable, ISerializable
    {
        private readonly String _sourceTableName;
        private bool _fromDatabase;

        private IndexesHelper _indexes = new IndexesHelper();


        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="sourceTableName"></param>
        /// <param name="fromDatabase"></param>
        internal TableDefHelper(string name, string sourceTableName, bool fromDatabase)
            : base(name)
        {
            _sourceTableName = sourceTableName;
            FromDatabase = fromDatabase;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="sourceTableName"></param>
        public TableDefHelper(string name, string sourceTableName)
            : this(name, sourceTableName, false)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        protected TableDefHelper(
         SerializationInfo info, StreamingContext context)
            : base(info, context)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        [SecurityPermission(SecurityAction.Demand,
           SerializationFormatter = true)]
        public override void GetObjectData(
           SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("Name", Name);
        }

        /// <summary>
        /// 
        /// </summary>
        public IndexesHelper Indexes
        {
            get { return _indexes; }
            set { _indexes = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="dbtype"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public DataColumn CreateField(string columnName, DbType dbtype, object size)
        {
            DataColumn field = new DataColumn();
            field.ColumnName = columnName;
            field.DataType = DbTypesConverter.DbTypeToType(dbtype);
            if (size != null)
            {
                field.MaxLength = int.Parse(size.ToString());
            }
            return field;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public IndexHelper CreateIndex(string name)
        {
            IndexHelper index = new IndexHelper();
            index.Name = name;
            return index;
        }


        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get { return _sourceTableName; }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool FromDatabase
        {
            get { return _fromDatabase; }
            set { _fromDatabase = value; }
        }
    }
}

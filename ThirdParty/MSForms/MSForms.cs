﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace UpgradeHelpers.MSForms
{
    public class MSCombobox : Telerik.WinControls.UI.RadDropDownList
    {
        public MSCombobox()
            : base()
        {
            InitializeComponent();
        }

        public void InitializeComponent()
        {

        }

        public MSCombobox(IContainer container): base()
        {
            container.Add(this);

            InitializeComponent();
        }

        public string ColumnWidths
        {
            get
            {
                return "";
            }
            set
            {
            }
        }

        public int ListCount
        {
            get
            {
                return this.Items.Count;
            }
        }

        public object get_Column(object tempRefParam3, object tempRefParam4)
        {
            
            object item = null;
            if(Convert.ToInt16(tempRefParam4) >= 0 && (int)tempRefParam3 == 1 )
                item = this.Items[(int)tempRefParam4].Value;
            else if(Convert.ToInt16(tempRefParam4) >= 0 && (int)tempRefParam3 == 0)
                item = this.Items[(int)tempRefParam4].Text;
            return item;
        }

        public void set_List(object tempRefParam3, object tempRefParam4, object tempRefParam5)
        {
            this.Items[(int)tempRefParam3].Value = tempRefParam5;
        }

        public int get_ListIndex()
        {
            return this.SelectedIndex;
        }

        public RadListDataItem GetListItem(int i)
        {
            return this.Items[i];
        }

        public void AddItem(object tempRefParam)
        {
            AddItem(tempRefParam, null);
        }

        public void AddItem(object tempRefParam, object tempRefParam2)
        {
            if (tempRefParam != null)
            {
                RadListDataItem item = new RadListDataItem();
                item.Text = (string)tempRefParam;
                if (tempRefParam2 != null)
                    item.Value = tempRefParam2;
                else
                    item.Value = tempRefParam;
                this.Items.Add(item);
            }
        }

        public void set_Column(int tempRefParam, int tempRefParam2, string tempRefParam3)
        {
            this.Items[tempRefParam2].Value = tempRefParam3;
        }

        public void set_ListIndex(int I)
        {
            this.SelectedIndex = I;
        }

        public void Clear()
        {
            this.Items.Clear();
        }

        public string GetItemData(int indice)
        {
           if (indice <= this.Items.Count())
              return Convert.ToString(this.Items[indice].Value);

           return "";            
        }

        public void SetItemData(int indice,object val)
        {
            if (indice <= this.Items.Count())
                this.Items[indice].Value = val;
            //this.Items[indice].Value = (string)val;
        }

        public object get_Value()
        {
            return this.SelectedValue;
        }
        public int GetNewIndex()
        {
            if(Items.Count == 0)
                this.Items.Add("");
            return this.Items.Count-1;
        }

        public string get_List(int indice, int textOrValue)
        {
            if (indice <= this.Items.Count)
            {
                if (textOrValue == 0)
                {
                    return this.Items[indice].Text;
                }
                else 
                {
                    return Convert.ToString(this.Items[indice].Value);
                }
            }
            return "";

        }

    }
    public class MSListBox : Telerik.WinControls.UI.RadGridView
    {
        public MSListBox()
            : base()
        {
            InitializeComponent();
            }

        public void InitializeComponent()
        {
            this.ShowGroupPanel = false;
            this.MasterTemplate.ShowColumnHeaders = true;
            this.MasterTemplate.AllowAddNewRow = false;
            this.MasterTemplate.ShowRowHeaderColumn = false;
            this.ReadOnly = true;
        }

        public MSListBox(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        /// <summary>
        /// Valida que un indice de fila este en la colección
        /// </summary>
        /// <param name="fila">indice de fila a validar</param>
        /// <returns></returns>
        private bool validaFila(int fila)
        {
            if (this.Rows.Count > fila)
                return true;
            return false;
        }

        /// <summary>
        /// Valida que un indice de columna este en la colección
        /// </summary>
        /// <param name="columna">indice de columna a validar</param>
        /// <returns></returns>
        private bool validaColumna(int columna)
        {
            if (this.Columns.Count > columna)
                return true;
            return false;
        }

        /// <summary>
        /// Obtiene/asigna cantidad de columnas
        /// </summary>
        public int ColumnCount
        {
            get
            {
                return this.Columns.Count;
            }

            set
            {
                for (int i = 0; i < value; i++)
                {
                    this.Columns.Add(i.ToString());
                }
            }
        }

        /// <summary>
        /// Asigna anchos de columnas
        /// </summary>
        public String ColumnWidths
        {
            set
            {
                string[] temp = value.Split(';');

                if (temp.Count() == this.ColumnCount)
                {
                    for (int i = 0; i < temp.Count(); i++)
                    {
                        this.Columns[i].Width = Convert.ToInt32(temp[i]);

                        if (Convert.ToInt32(temp[i]) == 0)
                            this.Columns[i].IsVisible = false;
                    }
                }
            }
        }

        /// <summary>
        /// Obtiene la cantidad de filas que tiene el control
        /// </summary>
        public int ListCount
        {
            get
            {
                return this.Rows.Count;
            }           
        }

        /// <summary>
        /// Adiciona una fila a la colección
        /// </summary>
        /// <param name="tempRefParam"></param>
        /// <param name="tempRefParam2"></param>
        public void AddItem(object tempRefParam, object tempRefParam2)
        {
            if (this.Columns.Count > 0)
            {
                GridViewDataRowInfo rowInfo = new GridViewDataRowInfo(this.MasterView);
                rowInfo.Cells[0].Value = tempRefParam;
                this.Rows.Add(rowInfo);
                this.Rows[0].IsCurrent = true;
                this.CurrentRow = null;
            }
        }

        /// <summary>
        /// Asigna un valor a una celda
        /// </summary>
        /// <param name="columna">Indice de la columna de la celda</param>
        /// <param name="fila">indice de la fila de la celda</param>
        /// <param name="value">Valor a asignar a la celda</param>
        public void set_Column(int columna, int fila, string value)
        {
            if (validaFila(fila) && validaColumna(columna))
                this.Rows[fila].Cells[columna].Value = value;
        }

        /// <summary>
        /// Verifica si una fila esta seleccionada
        /// </summary>
        /// <param name="fila">Indice de la fila a verificar</param>
        /// <returns></returns>
        public bool get_Selected(int fila)
        {
            if (validaFila(fila) && this.Rows[fila].IsSelected)
                return true;
            return false;
        }      

        /// <summary>
        /// Recupera el valor de una celda
        /// </summary>
        /// <param name="fila">Indice de la fila de la celda</param>
        /// <param name="columna">Indice de la fila de la columna</param>
        /// <returns></returns>
        public string get_List(object fila, object columna)
        {
            if (validaFila(Convert.ToInt32(fila)))
            {
              if (columna == Type.Missing)
                 return this.Rows[Convert.ToInt32(fila)].Cells[0].Value.ToString();
              else if (columna != Type.Missing && validaColumna(Convert.ToInt32(columna)))
                    return this.Rows[Convert.ToInt32(fila)].Cells[Convert.ToInt32(columna)].Value.ToString();
            }
            return "";

        }

        /// <summary>
        /// Recupera el valor de una celda
        /// </summary>
        /// <param name="columna">Indice de la fila de la columna</param>
        /// <param name="fila">Indice de la fila de la celda</param>
        /// <returns></returns>
        public string get_Column(int columna, int fila)
        {
            if (validaFila(fila) && validaColumna(columna))
                return Convert.ToString(this.Rows[fila].Cells[columna].Value);
            return "";
        }

        public int get_ListIndex()
        {
            if (this.SelectedRows.Count > 0)
                return ((GridViewSelectedRowsCollection)this.SelectedRows)[0].Index;
            return -1;
        }

        /// <summary>
        /// Asigna un valor a una celda
        /// </summary>
        /// <param name="fila">Indice de la fila de la celda</param>
        /// <param name="columna">Indice de la columna de la celda</param>
        /// <param name="value">Valor a asignar</param>
        public void set_List(int fila, int columna, string value)
        {
            if (validaFila(fila) && validaColumna(columna))
                this.Rows[fila].Cells[columna].Value = value;
        }
        
        /// <summary>
        /// Remueve una fila de la colección
        /// </summary>
        /// <param name="fila">indice de la fila a eliminar</param>
        public void RemoveItem(int fila)
        {
            if (validaFila(fila))
            {
                this.Rows[fila].Delete();
                this.CurrentRow = null;
            }
        }

        /// <summary>
        /// Limpia todas las filas de la colección
        /// </summary>
        public void Clear()
        {
            this.Rows.Clear();
        }

        /// <summary>
        /// Selecciona/deselecciona una fila
        /// </summary>
        /// <param name="fila">Fila a selecciona/deseleccionar</param>
        /// <param name="seleccionar">Selecciona/deselecciona</param>
        public void set_Selected(int fila, bool seleccionar)
        {
            if (validaFila(fila))
                this.Rows[fila].IsSelected = seleccionar;
        }

        /// <summary>
        /// Ubica el indice de la coleccion en una fila determinada
        /// </summary>
        /// <param name="fila">Indice de la fila</param>
        public void set_ListIndex(int fila)
        { 
            if (validaFila(fila))
                this.CurrentRow = this.Rows[fila];
        }
    }    
}


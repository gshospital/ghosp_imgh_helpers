using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Transactions;
using Telerik.WinControls.UI;
using System.IO;
using Telerik.WinControls;

namespace UpgradeStubs
{
    public class RDO_rdoErrors
    {
        private SqlException sqlException;
        public RDO_rdoErrors(SqlException sqlException)
        {
            this.sqlException = sqlException;
        }

        public void Clear()
        {
            // En ADO.NET, no se tiene implementaci�n de metodo Clear para limpiar los errores generados.
        }
        public int getCount()
        {
            return sqlException.Errors.Count;
        }
        public SqlError Item(int Index)
        {
            return sqlException.Errors[Index];
        }
        public System.Collections.IEnumerator GetEnumerator()
        {
            return sqlException.Errors.GetEnumerator();
        }
        public SqlErrorCollection rdoErrors { get { return sqlException.Errors; } }
    }
    public static class System_Data_DataSet
    {
        private static int _index;
        public static void AddNew(this DataSet instance)
        {
            //SDSALAZAR_TODO_SPRINT_1
            //IMPLEMENTACI�N UPGRADESTUBS
            DataRow dre = instance.Tables[0].NewRow();

            instance.Tables[0].NewRow();
            instance.Tables[0].Rows.InsertAt(dre, 0);
        }
        public static void Close(this DataSet instance)
        {
            //SDSALAZAR_TODO_SPRINT_1
            //IMPLEMENTACI�N UPGRADESTUBS
            instance.Clear();
            instance.Dispose();
        }
        public static void setAbsolutePosition(this DataSet instance, int valor)
        {
            
        }
        public static void Edit(this DataSet instance)
        {
        }
        /// <summary>
        /// Elimina todos los DataRow de la tabla 0 de un dataset
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="sqlda">DataAdapter para realizar el update al dataset</param>
        public static void Delete(this DataSet instance, SqlDataAdapter sqlda)
        {
            //Indra jproche 05/04/2016
            foreach (DataRow dr in instance.Tables[0].Rows)
            {
                dr.Delete();
            }
            SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(sqlda);
            sqlda.Update(instance);
        }
        /// <summary>
        /// indra - ralopezn - 06/04/2016
        /// M�todo que permite eliminar el datarow especifico al indice de entrada
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="sqlda">DataAdapter para realizar el update al dataset</param>
        /// <param name="index">posici�n donde se eliminar� del registro</param>
        public static void Delete(this DataSet instance, SqlDataAdapter sqlda, int index)
        {
            instance.Tables[0].Rows[index].Delete();
            SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(sqlda);
            sqlda.Update(instance);
        }
        public static int MovePrevious(this DataSet instance)
        {
            if (_index < instance.Tables[0].Rows.Count && _index != 0)
            {
                --_index;
            }
            return _index;
        }
        public static int MoveFirst(this DataSet instance)
        {
            _index = 0;
            return _index;
        }

        /// <summary>
        /// indra - ralopezn - 06/04/2016
        /// M�todo que permite obtener la ultima posici�n del index.
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="Options">opciones</param>
        /// <returns>Indice de la posicion del registro</returns>
        public static int MoveLast(this DataSet instance, object Options)
        {
            _index = instance.Tables[0].Rows.Count - 1;
            return _index;
        }
        public static int MoveNext(this DataSet instance)
        {
            if (_index < instance.Tables[0].Rows.Count)
            {
                ++_index;
            }
            return _index;
        }

        public static void CreateStructTable(this DataSet instance,string tableName, params string[] columnas)
        {
            foreach (string value in columnas)
            {
                instance.Tables[tableName].Columns.Add(value);
            }

        }

        public static void CreateTable(this DataSet instance, string tableName)
        {
            CreateTable(instance, tableName, null);
        }

        public static void CreateTable(this DataSet instance, string tableName, string pathXmlSchema)
        {
            DataTable table = null;
            if (!instance.Tables.Contains(tableName))
            {
                table = new DataTable();

                if (!string.IsNullOrEmpty(pathXmlSchema) && File.Exists(pathXmlSchema))
                {
                    table.ReadXmlSchema(pathXmlSchema);
                    table.TableName = tableName;
                    table.ReadXml(pathXmlSchema);
                }
                else if (!File.Exists(pathXmlSchema))
                {
                    RadMessageBox.Show("No se encuentra el esquema en la ruta especificada");
                }
                
                instance.Tables.Add(table);
            }
            else
            {
                RemoveTable(instance, tableName);
                CreateTable(instance, tableName, pathXmlSchema);
            }
        }

        public static void RemoveTable(this DataSet instance, string tableName)
        {
            if (!instance.Tables.Contains(tableName))
                return;

            DataTable table = instance.Tables[tableName];
            if (instance.Tables.CanRemove(table))
            {
                instance.Tables.Remove(table);
            }
        }

        public static void RemoveTables(this DataSet instance)
        {
            foreach (DataTable table in instance.Tables)
            {
                RemoveTable(instance, table.TableName);
            }
        }
    }

    /// <summary>
    /// indra - ralopezn - 11/05/2016
    /// Clase que permite construir el objeto de transactionScope con nivel
    /// de aislamiento.
    /// </summary>
    public static class TransScopeBuilder
    {
        /// <summary>
        /// indra - ralopezn - 11/05/2016
        /// M�todo que permite crear la transacci�n con nivel de aislamiento "ReadCommitted".
        /// </summary>
        /// <returns>objeto de tipo TransactionScope</returns>
        public static TransactionScope CreateReadCommited()
        {
            var optionsTransaction = new TransactionOptions {
                IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted,
                Timeout = TransactionManager.DefaultTimeout
            };

            return new TransactionScope(TransactionScopeOption.Required, optionsTransaction);
        }
    }

    public static class System_Data_SqlClient_SqlCommand
    {
        public static void Close(this SqlCommand instance)
        {
            instance.Dispose();
        }

    }
    public static class System_Data_SqlClient_SqlConnection
    {
        private static System.Collections.Generic.IDictionary<string, SqlCommand> _dictionary = new System.Collections.Generic.Dictionary<string, SqlCommand>();
        private static SqlTransaction _sqlTransaction;
        private static TransactionScope _scope;

        /// <summary>
        ///Crea una instancia de TransactionScope
        ///INDRA - jproche - 13/04/2016
        /// </summary>
        /// <param name="instance"></param>
        public static void BeginTransScope(this SqlConnection instance)
        {
            bool isTransactionScopeDisposed = false;
            if (_scope != null)
            {
                System.Reflection.FieldInfo disposed = _scope.GetType().GetField("disposed",
                         System.Reflection.BindingFlags.NonPublic |
                         System.Reflection.BindingFlags.Instance);
                isTransactionScopeDisposed = (bool)disposed.GetValue(_scope);
            }


            if (_scope == null || isTransactionScopeDisposed)
            {
                _scope = TransScopeBuilder.CreateReadCommited();
                if (instance.State == ConnectionState.Open)
                {
                    instance.Close();
                    instance.Open();
                }
            }
        }

        /// <summary>
        /// Realiza un Commit (complete) del TransactionScope
        /// INDRA - jproche - 13/04/2016
        /// </summary>
        /// <param name="instance"></param>
        public static void CommitTransScope(this SqlConnection instance)
        {
            if (_scope != null)
            {
                _scope.Complete();
                _scope.Dispose();
                if (instance.State == ConnectionState.Open)
                {
                    instance.Close();
                    instance.Open();
                }
            }
        }

        /// <summary>
        /// Realiza un Rollback (dispose) del TransactionScope
        /// INDRA - jproche - 13/04/2016
        /// </summary>
        /// <param name="instance"></param>
        public static void RollbackTransScope(this SqlConnection instance)
        {
            bool isTransactionScopeDisposed = false;
            if (_scope != null)
            {
                System.Reflection.FieldInfo disposed = _scope.GetType().GetField("disposed",
                         System.Reflection.BindingFlags.NonPublic |
                         System.Reflection.BindingFlags.Instance);
                isTransactionScopeDisposed = (bool)disposed.GetValue(_scope);
            }


            if (_scope != null && !isTransactionScopeDisposed)
            {
                _scope.Dispose();
                if (instance.State == ConnectionState.Open)
                {
                    instance.Close();
                    instance.Open();
                }
            }
        }
        public static void DisposeTransScope(this SqlConnection instance)
        {
            RollbackTransScope(instance);
        }

        /// <summary>
        /// INDRA - ralopezn - 14/03/2016
        /// Se cambia el m�todo para que permita retornar la transacci�n creada para manejarla
        /// en los diferentes m�todos de SqlClient.
        /// </summary>
        /// <returns>Objeto de Transacci�n</returns>
        public static SqlTransaction BeginTrans(this SqlConnection instance)
        {
            _sqlTransaction = instance.BeginTransaction("SampleTransaction");
            return _sqlTransaction;
        }
        public static void CommitTrans(this SqlConnection instance, SqlTransaction transaction = null)
        {
            if (_sqlTransaction != null && transaction == null)
            {
                transaction = _sqlTransaction;
            }
            transaction.Commit();
        }
        /// <summary>
        /// indra - ralopezn - 05/04/2016
        /// M�todo que permite preparar el query para recibir par�metros.
        /// La funcionalidad extrae los parametros que contenga la query e instancia el arreglo
        /// de los mismos. Todos los par�metros se entrega con valor nulo.
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="Name">Nombre del query a almacenar</param>
        /// <param name="SqlString">Sentencia Sql</param>
        /// <returns>Objeto SqlCommand con parametros instanciados (si los hay)</returns>
        public static SqlCommand CreateQuery(this SqlConnection instance, string Name, object SqlString)
        {
            string sqlStringCommand = SqlString.ToString();
            SqlCommand command = new SqlCommand(sqlStringCommand, instance, _sqlTransaction);

            Regex getParamRegex = new Regex(@"(?<Parameter>@\w*)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            var listaDeParametros = getParamRegex.Matches(sqlStringCommand);

            if (listaDeParametros.Count > 0)
            {
                foreach (string parametro in listaDeParametros)
                {
                    command.Parameters.AddWithValue(parametro, DBNull.Value);
                }
            }

            if (!_dictionary.ContainsKey(Name))
            {
                _dictionary.Add(Name, command);
            }
            else
            {
                _dictionary[Name] = command;
            }
            
            return command;
        }
        /// <summary>
        /// M�todo que permite ejecutar las query que est�n almacenadas con el m�todo
        /// CreateQuery.
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="Name">Nombre de la consulta almacenada</param>
        /// <returns>Retorna los datos consultados</returns>
        public static DataSet ExecuteQuery(this SqlConnection instance, string Name)
        {
            DataSet resultSet = new DataSet();
            if (_dictionary.ContainsKey(Name))
            {
                SqlCommand command = _dictionary[Name];
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(resultSet);
            }

            return resultSet;
        }
        /// <summary>
        /// Sobrecarga del m�todo. M�todo que permite ejecutar las query que est�n almacenadas con el m�todo
        /// CreateQuery y limita la cantidad de registros a obtener.
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="Name">Nombre de la consulta almacenada</param>
        /// <param name="startRecord">Inicio de los registros a obtener</param>
        /// <param name="maxRecords">Cantidad m�xima de registro a mostrar</param>
        /// <returns>Retorna los datos consultados</returns>
        public static DataSet ExecuteQuery(this SqlConnection instance, string Name, int startRecord, int maxRecords)
        {
            DataSet resultSet = new DataSet();
            if (_dictionary.ContainsKey(Name))
            {
                SqlCommand command = _dictionary[Name];
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(resultSet, startRecord, maxRecords, Name);
            }
            return resultSet;
        }
        public static string getConnect(this SqlConnection instance)
        {
            return instance.ConnectionString;
        }
        public static string getName(this SqlConnection instance)
        {
            //dgmorenog
            return instance.DataSource;
        }
        /// <summary>
        /// INDRA - ralopezn - 14/03/2016
        /// Se implementa metodo rollback de acuerdo al objeto transacci�n que se le pasa.
        /// </summary>
        /// <param name="transaction"></param>
        public static void RollbackTrans(this SqlConnection instance, SqlTransaction transaction = null)
        {
            if (_sqlTransaction != null && transaction == null)
            {
                transaction = _sqlTransaction;
            }
            transaction.Rollback();
        }
        public static bool getStillExecuting(this SqlConnection instance)
        {
            return false;
        }
        public static int getQueryTimeout(this SqlConnection instance)
        {
            int queryTimeout = 30;
            string tstTimeout = "select nnumeri1 from sconsglo where gconsglo='TTIMEOUT'";
            SqlDataAdapter tempAdapter = new SqlDataAdapter(tstTimeout, instance);
            DataSet tRrTimeout = new DataSet();
            tempAdapter.Fill(tRrTimeout);
            if (tRrTimeout.Tables[0].Rows.Count != 0)
            {
                if (Convert.ToDouble(tRrTimeout.Tables[0].Rows[0]["nnumeri1"]) >= 30)
                {
                    //Nombre_Conexion.setQueryTimeout(Convert.ToInt32(tRrTimeout.Tables[0].Rows[0]["NNUMERI1"]));
                    queryTimeout = Convert.ToInt32(tRrTimeout.Tables[0].Rows[0]["NNUMERI1"]);
                }
                else
                {
                    //Nombre_Conexion.setQueryTimeout(30);
                    queryTimeout = 30;
                }
            }
            else
            {
                //Nombre_Conexion.setQueryTimeout(30);
                queryTimeout = 30;
            }
            tRrTimeout.Close();
            return queryTimeout;
        }

        public static void setQueryTimeout(this SqlConnection instance, int QueryTimeout)
        {
            //UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("RDO.rdoConnection.QueryTimeout");
        }

        /// <summary>
        /// INDRA - jproche - 06/04/2016
        /// </summary>
        /// <param name="instance"></param>
        /// <returns>La transaccion actual</returns>
        public static SqlTransaction getCurrentTransaction(this SqlConnection instance)
        {
            return _sqlTransaction;
        }
    }

    public static class System_Data_DataRow
    {
        
        public static Type getType(this DataRow instance, int index)
        {
            return instance[index].GetType();
        }
    }

    public static class System_Windows_Forms_Control
    {
        public static string FormattedText(this Control instance)
        {
            return "";
        }

        public static string Mask(this Control instance)
        {
            string retorno = string.Empty;
            if (instance is RadMaskedEditBox)
            {
                var masked = (RadMaskedEditBox)instance;
                retorno = masked.Mask;
            }

            return retorno;
        }
    }
}
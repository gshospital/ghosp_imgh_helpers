using System.Runtime.InteropServices;

namespace UpgradeSupportHelper.PInvoke.UnsafeNative
{
    [System.Security.SuppressUnmanagedCodeSecurity]
	public static class menu
	{

		[DllImport("Menu.dll", EntryPoint = "CallProcIndirect", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int DllRegisterServer(int direccion);
		[DllImport("Menu.dll", EntryPoint = "CallProcIndirect", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int DllUnRegisterServer(int direccion);
	}
}
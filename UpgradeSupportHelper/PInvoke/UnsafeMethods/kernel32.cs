using System.Runtime.InteropServices;

namespace UpgradeSupportHelper.PInvoke.UnsafeNative
{
    [System.Security.SuppressUnmanagedCodeSecurity]
	public static class kernel32
	{

		[DllImport("kernel32.dll", EntryPoint = "CreateToolhelp32Snapshot", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int CreateToolhelpSnapshot(int lFlags, int lProcessID);
		[DllImport("KERNEL32.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int FreeLibrary(int hLibModule);
		[DllImport("kernel32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int GetCurrentProcessId();
		[DllImport("kernel32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int GetExitCodeProcess(int hProcess, ref int lpExitCode);
		[DllImport("kernel32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int GetProcAddress(int hModule, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpProcName);
		[DllImport("kernel32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int GetProcessHeap();
		[DllImport("kernel32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int CloseHandle(int hObject);
		[DllImport("kernel32.dll", EntryPoint = "RtlMoveMemory", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static void CopyMemory(System.IntPtr Destination, System.IntPtr Source, int Length);
		[DllImport("kernel32.dll", EntryPoint = "DeleteFileA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int DeleteFile([MarshalAs(UnmanagedType.VBByRefStr)] ref string lpFileName);
		[DllImport("kernel32.dll", EntryPoint = "GetComputerNameA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int GetComputerName([MarshalAs(UnmanagedType.VBByRefStr)] ref string lpbuffer, ref int nSize);
		[DllImport("kernel32.dll", EntryPoint = "GetProfileStringA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int GetProfileString([MarshalAs(UnmanagedType.VBByRefStr)] ref string lpAppName, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpKeyName, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpDefault, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpReturnedString, int nSize);
		[DllImport("kernel32.dll", EntryPoint = "GetSystemDirectoryA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int GetSystemDirectory([MarshalAs(UnmanagedType.VBByRefStr)] ref string lpbuffer, int nSize);
		//UPGRADE_TODO: (1050) Structure OSVERSIONINFO may require marshalling attributes to be passed as an argument in this Declare statement. More Information: http://www.vbtonet.com/ewis/ewi1050.aspx
		[DllImport("kernel32.dll", EntryPoint = "GetVersionExA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int GetVersionEx(ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.OSVERSIONINFO lpVersionInformation);
		[DllImport("kernel32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int HeapAlloc(int hHeap, int dwFlags, int dwBytes);
		[DllImport("kernel32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int HeapFree(int hHeap, int dwFlags, int lpMem);
		[DllImport("KERNEL32.DLL", EntryPoint = "LoadLibraryA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int LoadLibrary([MarshalAs(UnmanagedType.VBByRefStr)] ref string lpLibFileName);
		[DllImport("kernel32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int OpenProcess(int dwDesiredAccess, int bInheritHandle, int dwProcessId);
		//UPGRADE_TODO: (1050) Structure PROCESSENTRY32 may require marshalling attributes to be passed as an argument in this Declare statement. More Information: http://www.vbtonet.com/ewis/ewi1050.aspx
		[DllImport("kernel32.dll", EntryPoint = "Process32First", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int ProcessFirst(int hSnapShot, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.PROCESSENTRY32 uProcess);
		[DllImport("kernel32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int ProcessIdToSessionId(int dwProcessId, ref int pSessionId);
		[DllImport("kernel32.dll", EntryPoint = "Process32Next", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int ProcessNext(int hSnapShot, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.PROCESSENTRY32 uProcess);
		[DllImport("kernel32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static void RtlMoveMemory(System.IntPtr hpvDest, int hpvSource, int cbCopy);
		[DllImport("kernel32.dll", EntryPoint = "SetLocaleInfoA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int SetLocaleInfo(int Locale, int LCType, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpLCData);
		[DllImport("kernel32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static void Sleep(int dwMilliseconds);
		[DllImport("kernel32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int TerminateProcess(int hProcess, int uExitCode);
		[DllImport("kernel32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int WaitForSingleObject(int hHandle, int dwMilliseconds);
		[DllImport("kernel32.dll", EntryPoint = "WriteProfileStringA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int WriteProfileString([MarshalAs(UnmanagedType.VBByRefStr)] ref string lpszSection, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpszKeyName, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpszString);
        [DllImport("kernel32.dll", EntryPoint = "GetLocaleInfoA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        extern public static int GetLocaleInfo(int Locale, int LCType, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpLCData, int cchData);
    }
}
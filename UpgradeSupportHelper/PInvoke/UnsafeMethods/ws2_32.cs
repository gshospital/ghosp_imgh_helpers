using System.Runtime.InteropServices;

namespace UpgradeSupportHelper.PInvoke.UnsafeNative
{
    [System.Security.SuppressUnmanagedCodeSecurity]
	public static class ws2_32
	{

		[DllImport("ws2_32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int gethostbyname([MarshalAs(UnmanagedType.VBByRefStr)] ref string host_name);
		[DllImport("ws2_32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int gethostname([MarshalAs(UnmanagedType.VBByRefStr)] ref string host_name, int namelen);
	}
}
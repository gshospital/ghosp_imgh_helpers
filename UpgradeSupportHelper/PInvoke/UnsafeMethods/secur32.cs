using System.Runtime.InteropServices;

namespace UpgradeSupportHelper.PInvoke.UnsafeNative
{
    [System.Security.SuppressUnmanagedCodeSecurity]
	public static class secur32
	{

		//UPGRADE_TODO: (1050) Structure TimeStamp may require marshalling attributes to be passed as an argument in this Declare statement. More Information: http://www.vbtonet.com/ewis/ewi1050.aspx
		[DllImport("secur32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int AcceptSecurityContext(ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecHandle phCredential, System.IntPtr phContext, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecBufferDesc pInput, int fContextReq, int TargetDataRep, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecHandle phNewContext, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecBufferDesc pOutput, ref int pfContextAttr, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.TimeStamp ptsExpiry);
		[DllImport("secur32.dll", EntryPoint = "AcquireCredentialsHandleA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int AcquireCredentialsHandle(int pszPrincipal, [MarshalAs(UnmanagedType.VBByRefStr)] ref string pszPackage, int fCredentialUse, int pvLogonId, System.IntPtr pAuthData, int pGetKeyFn, int pvGetKeyArgument, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecHandle phCredential, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.TimeStamp ptsExpiry);
		//UPGRADE_TODO: (1050) Structure SecBufferDesc may require marshalling attributes to be passed as an argument in this Declare statement. More Information: http://www.vbtonet.com/ewis/ewi1050.aspx
		[DllImport("secur32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int CompleteAuthToken(ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecHandle phContext, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecBufferDesc pToken);
		//UPGRADE_TODO: (1050) Structure SecHandle may require marshalling attributes to be passed as an argument in this Declare statement. More Information: http://www.vbtonet.com/ewis/ewi1050.aspx
		[DllImport("secur32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int DeleteSecurityContext(ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecHandle phContext);
		[DllImport("secur32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int FreeContextBuffer(int pvContextBuffer);
		[DllImport("secur32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int FreeCredentialsHandle(ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecHandle phContext);
		[DllImport("secur32.dll", EntryPoint = "InitializeSecurityContextA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int InitializeSecurityContext(System.IntPtr phCredential, System.IntPtr phContext, int pszTargetName, int fContextReq, int Reserved1, int TargetDataRep, System.IntPtr pInput, int Reserved2, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecHandle phNewContext, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecBufferDesc pOutput, ref int pfContextAttr, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.TimeStamp ptsExpiry);
		[DllImport("secur32.dll", EntryPoint = "QuerySecurityPackageInfoA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int QuerySecurityPackageInfo([MarshalAs(UnmanagedType.VBByRefStr)] ref string PackageName, ref int pPackageInfo);
	}
}
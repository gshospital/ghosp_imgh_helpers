using System.Runtime.InteropServices;

namespace UpgradeSupportHelper.PInvoke.UnsafeNative
{
    [System.Security.SuppressUnmanagedCodeSecurity]
	public static class vb5stkit
	{

		[DllImport("VB5STKIT.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static short DLLSelfRegister([MarshalAs(UnmanagedType.VBByRefStr)] ref string lpDllName);
	}
}
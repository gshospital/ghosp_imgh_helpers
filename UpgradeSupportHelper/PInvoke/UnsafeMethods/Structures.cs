using System;
using System.Runtime.InteropServices;
using UpgradeHelpers.Helpers;

namespace UpgradeSupportHelper.PInvoke.UnsafeNative
{
    [System.Security.SuppressUnmanagedCodeSecurity]
    public static class Structures
    {

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct OSVERSIONINFO
        {
            public int OSVSize;
            public int dwVerMajor;
            public int dwVerMinor;
            public int dwBuildNumber;
            public int PlatformID;
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst = 128)]
            public char[] _szCSDVersion;
            public string szCSDVersion
            {
                get
                {
                    return new string(_szCSDVersion);
                }
                set
                {
                    MemoryHelper.CopyValueToArray(_szCSDVersion, value);
                }
            }

            private static void InitStruct(ref OSVERSIONINFO result, bool init)
            {
                result._szCSDVersion = new char[128];
            }
            public static OSVERSIONINFO CreateInstance()
            {
                OSVERSIONINFO result = new OSVERSIONINFO();
                InitStruct(ref result, true);
                return result;
            }
            public OSVERSIONINFO Clone()
            {
                OSVERSIONINFO result = this;
                InitStruct(ref result, false);
                Array.Copy(this._szCSDVersion, result._szCSDVersion, 128);
                return result;
            }
        }
        [StructLayout(LayoutKind.Sequential)]
        public struct POINTAPI
        {
            public int X;
            public int Y;
        }
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct PROCESSENTRY32
        {
            public int dwSize;
            public int cntUsage;
            public int th32ProcessID;
            public int th32DefaultHeapID;
            public int th32ModuleID;
            public int cntThreads;
            public int th32ParentProcessID;
            public int pcPriClassBase;
            public int dwFlags;
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst = 260)]
            public char[] _szExeFile;
            public string szExeFile
            {
                get
                {
                    return new string(_szExeFile);
                }
                set
                {
                    MemoryHelper.CopyValueToArray(_szExeFile, value);
                }
            }

            private static void InitStruct(ref PROCESSENTRY32 result, bool init)
            {
                result._szExeFile = new char[260];
            }
            public static PROCESSENTRY32 CreateInstance()
            {
                PROCESSENTRY32 result = new PROCESSENTRY32();
                InitStruct(ref result, true);
                return result;
            }
            public PROCESSENTRY32 Clone()
            {
                PROCESSENTRY32 result = this;
                InitStruct(ref result, false);
                Array.Copy(this._szExeFile, result._szExeFile, 260);
                return result;
            }
        }
        [StructLayout(LayoutKind.Sequential)]
        public struct SecBufferDesc
        {
            public int ulVersion;
            public int cBuffers;
            public int pBuffers;
        }
        [StructLayout(LayoutKind.Sequential)]
        public struct SecHandle
        {
            public int dwLower;
            public int dwUpper;
        }
        [StructLayout(LayoutKind.Sequential)]
        public struct TimeStamp
        {
            public int LowPart;
            public int HighPart;
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct NOTIFYICONDATA
        {
            public int cbSize;
            public int hwnd;
            public int uID;
            public int uFlags;
            public int uCallbackMessage;
            public int hIcon;
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst = 128)]
            private char[] _szTip;
            public int dwState;
            public int dwStateMask;
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst = 256)]
            private char[] _szInfo;
            public int uTimeout;
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst = 64)]
            private char[] _szInfoTitle;
            public int dwInfoFlags;
            public string szTip
            {
                get
                {
                    return new string(_szTip);
                }
                set
                {
                    MemoryHelper.CopyValueToArray(_szTip, value);
                }
            }

            public string szInfo
            {
                get
                {
                    return new string(_szInfo);
                }
                set
                {
                    MemoryHelper.CopyValueToArray(_szInfo, value);
                }
            }

            public string szInfoTitle
            {
                get
                {
                    return new string(_szInfoTitle);
                }
                set
                {
                    MemoryHelper.CopyValueToArray(_szInfoTitle, value);
                }
            }

            private static void InitStruct(ref NOTIFYICONDATA result, bool init)
            {
                result._szTip = new char[128];
                result._szInfo = new char[256];
                result._szInfoTitle = new char[64];
            }
            public static NOTIFYICONDATA CreateInstance()
            {
                NOTIFYICONDATA result = new NOTIFYICONDATA();
                InitStruct(ref result, true);
                return result;
            }
            public NOTIFYICONDATA Clone()
            {
                NOTIFYICONDATA result = this;
                InitStruct(ref result, false);
                Array.Copy(this._szTip, result._szTip, 128);
                Array.Copy(this._szInfo, result._szInfo, 256);
                Array.Copy(this._szInfoTitle, result._szInfoTitle, 64);
                return result;
            }
        }
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct NOTIFYICONDATA_Renamed
        {
            public int cbSize;
            public int hwnd;
            public int uID;
            public int uFlags;
            public int uCallbackMessage;
            public int hIcon;
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst = 64)]
            private char[] _szTip;
            public string szTip
            {
                get
                {
                    return new string(_szTip);
                }
                set
                {
                    MemoryHelper.CopyValueToArray(_szTip, value);
                }
            }

            private static void InitStruct(ref NOTIFYICONDATA_Renamed result, bool init)
            {
                result._szTip = new char[64];
            }
            public static NOTIFYICONDATA_Renamed CreateInstance()
            {
                NOTIFYICONDATA_Renamed result = new NOTIFYICONDATA_Renamed();
                InitStruct(ref result, true);
                return result;
            }
            public NOTIFYICONDATA_Renamed Clone()
            {
                NOTIFYICONDATA_Renamed result = this;
                InitStruct(ref result, false);
                Array.Copy(this._szTip, result._szTip, 64);
                return result;
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct OPENFILENAME
        {
            public int nStructSize;
            public int hWndOwner;
            public int hInstance;
            public string sFilter;
            public string sCustomFilter;
            public int nMaxCustFilter;
            public int nFilterIndex;
            public string sFile;
            public int nMaxFile;
            public string sFileTitle;
            public int nMaxTitle;
            public string sInitialDir;
            public string sDialogTitle;
            public int flags;
            public short nFileOffset;
            public short nFileExtension;
            public string sDefFileExt;
            public int nCustData;
            public int fnHook;
            public string sTemplateName;
            private static void InitStruct(ref OPENFILENAME result, bool init)
            {
                if (init)
                {
                    result.sFilter = String.Empty;
                    result.sCustomFilter = String.Empty;
                    result.sFile = String.Empty;
                    result.sFileTitle = String.Empty;
                    result.sInitialDir = String.Empty;
                    result.sDialogTitle = String.Empty;
                    result.sDefFileExt = String.Empty;
                    result.sTemplateName = String.Empty;
                }
            }
            public static OPENFILENAME CreateInstance()
            {
                OPENFILENAME result = new OPENFILENAME();
                InitStruct(ref result, true);
                return result;
            }
            public OPENFILENAME Clone()
            {
                OPENFILENAME result = this;
                InitStruct(ref result, false);
                return result;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace UpgradeSupportHelper.PInvoke.UnsafeNative
{
    class comdlg32
    {
        [DllImport("comdlg32.dll", EntryPoint = "GetOpenFileNameA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        extern public static int GetOpenFileName(ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.OPENFILENAME pOpenfilename);
    }
}

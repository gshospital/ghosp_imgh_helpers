using System.Runtime.InteropServices;

namespace UpgradeSupportHelper.PInvoke.UnsafeNative
{
    [System.Security.SuppressUnmanagedCodeSecurity]
	public static class wininet
	{

		[DllImport("wininet.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static short InternetCloseHandle(int hInet);
		[DllImport("wininet.dll", EntryPoint = "InternetOpenA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int InternetOpen([MarshalAs(UnmanagedType.VBByRefStr)] ref string sAgent, int lAccessType, [MarshalAs(UnmanagedType.VBByRefStr)] ref string sProxyName, [MarshalAs(UnmanagedType.VBByRefStr)] ref string sProxyBypass, int lFlags);
	}
}
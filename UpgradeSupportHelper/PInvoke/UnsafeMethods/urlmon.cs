using System.Runtime.InteropServices;

namespace UpgradeSupportHelper.PInvoke.UnsafeNative
{
    [System.Security.SuppressUnmanagedCodeSecurity]
	public static class urlmon
	{

		[DllImport("urlmon.dll", EntryPoint = "URLDownloadToFileA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int URLDownloadToFile(int pCaller, [MarshalAs(UnmanagedType.VBByRefStr)] ref string szURL, [MarshalAs(UnmanagedType.VBByRefStr)] ref string szFileName, int dwReserved, int lpfnCB);
	}
}
using System;
using System.Runtime.InteropServices;

namespace UpgradeSupportHelper.PInvoke.UnsafeNative
{
    [System.Security.SuppressUnmanagedCodeSecurity]
	public static class wtsapi32
	{

		[DllImport("wtsapi32.dll", EntryPoint = "WTSQuerySessionInformationA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        extern public static int WTSQuerySessionInformation(IntPtr hServer, int SessionID, int WTSInfoClass, out IntPtr ppBuffer, out int pBytesReturned);

        [DllImport("wtsapi32.dll", ExactSpelling = true, SetLastError = false)]
        public static extern void WTSFreeMemory(IntPtr memory);
	}
}
using System.Runtime.InteropServices;

namespace UpgradeSupportHelper.PInvoke.UnsafeNative
{
    [System.Security.SuppressUnmanagedCodeSecurity]
	public static class cfint32
	{

		[DllImport("CFINT32.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int nReleaseInstance(int Instance);
		[DllImport("CFINT32.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int nVBInitInstance(int hCmdButton, int EventMessage);
		[DllImport("CFINT32.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int nWritePacket([MarshalAs(UnmanagedType.VBByRefStr)] ref string InPacket, int Instance);
		[DllImport("CFINT32.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static string sVBReadPacket(int Instance);
	}
}
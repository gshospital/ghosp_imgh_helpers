using System;
using System.Runtime.InteropServices;

namespace UpgradeSupportHelper.PInvoke.UnsafeNative
{
    [System.Security.SuppressUnmanagedCodeSecurity]
	public static class shell32
	{

		[DllImport("shell32.dll", EntryPoint = "ShellExecuteA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int ShellExecute(IntPtr hwnd, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpOperation, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpFile, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpParameters, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpDirectory, int nShowCmd);


        [DllImport("shell32.dll", EntryPoint = "Shell_NotifyIconA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        extern public static int Shell_NotifyIcon(int dwMessage, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.NOTIFYICONDATA_Renamed lpData);
        //UPGRADE_TODO: (1050) Structure NOTIFYICONDATA may require marshalling attributes to be passed as an argument in this Declare statement. More Information: http://www.vbtonet.com/ewis/ewi1050.aspx
        [DllImport("shell32.dll", EntryPoint = "Shell_NotifyIconA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        extern public static bool Shell_NotifyIcon(int dwMessage, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.NOTIFYICONDATA pnid);
	}
}
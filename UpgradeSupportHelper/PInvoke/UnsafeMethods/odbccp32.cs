using System.Runtime.InteropServices;

namespace UpgradeSupportHelper.PInvoke.UnsafeNative
{
    [System.Security.SuppressUnmanagedCodeSecurity]
	public static class odbccp32
	{

		[DllImport("ODBCCP32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int SQLConfigDataSource(int hwndParent, int fRequest, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpszDriver, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpszAttributes);
	}
}
using System;

namespace UpgradeSupportHelper.PInvoke.SafeNative
{
    public static class shell32
	{

		public static int ShellExecute(IntPtr hwnd, ref string lpOperation, ref string lpFile, ref string lpParameters, ref string lpDirectory, int nShowCmd)
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.shell32.ShellExecute(hwnd, ref lpOperation, ref lpFile, ref lpParameters, ref lpDirectory, nShowCmd);
		}

        public static bool Shell_NotifyIcon(int dwMessage, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.NOTIFYICONDATA pnid)
        {
            return UpgradeSupportHelper.PInvoke.UnsafeNative.shell32.Shell_NotifyIcon(dwMessage, ref pnid);
        }
	}
}
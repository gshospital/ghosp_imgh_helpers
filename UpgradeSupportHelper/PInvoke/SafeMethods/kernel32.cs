using System;
using System.Runtime.InteropServices;

namespace UpgradeSupportHelper.PInvoke.SafeNative
{
    public static class kernel32
	{

		public static void RtlMoveMemory<T>(ref T hpvDest, int hpvSource, int cbCopy) where T : struct
		{
			GCHandle handle = GCHandle.Alloc(hpvDest, GCHandleType.Pinned);
			try
			{
				IntPtr tmpPtr = handle.AddrOfPinnedObject();
				//UPGRADE_WARNING: (8007) Trying to marshal a non Bittable Type (mbAcceso.HOSTENT). A special conversion might be required at this point. Moreover use 'External Marshalling attributes for Structs' feature enabled if required More Information: http://www.vbtonet.com/ewis/ewi8007.aspx
				UpgradeSupportHelper.PInvoke.UnsafeNative.kernel32.RtlMoveMemory(tmpPtr, hpvSource, cbCopy);
			}
			finally
			{
				handle.Free();
			}
		}
		public static int CloseHandle(int hObject)
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.kernel32.CloseHandle(hObject);
		}
		public static int CreateToolhelpSnapshot(int lFlags, int lProcessID)
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.kernel32.CreateToolhelpSnapshot(lFlags, lProcessID);
		}
		public static int DeleteFile(ref string lpFileName)
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.kernel32.DeleteFile(ref lpFileName);
		}
		public static int GetComputerName(ref string lpbuffer, ref int nSize)
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.kernel32.GetComputerName(ref lpbuffer, ref nSize);
		}
		public static int GetCurrentProcessId()
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.kernel32.GetCurrentProcessId();
		}
		public static int GetProcessHeap()
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.kernel32.GetProcessHeap();
		}
		public static int GetProfileString(ref string lpAppName, ref string lpKeyName, ref string lpDefault, ref string lpReturnedString, int nSize)
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.kernel32.GetProfileString(ref lpAppName, ref lpKeyName, ref lpDefault, ref lpReturnedString, nSize);
		}
		public static int GetSystemDirectory(ref string lpbuffer, int nSize)
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.kernel32.GetSystemDirectory(ref lpbuffer, nSize);
		}
		public static void CopyMemory<T, T2>(ref T Destination, int Source, int Length) where T : struct
		{
			GCHandle handle = GCHandle.Alloc(Destination, GCHandleType.Pinned);
			GCHandle handle2 = GCHandle.Alloc(Source, GCHandleType.Pinned);
			try
			{
				IntPtr tmpPtr2 = handle2.AddrOfPinnedObject();
				IntPtr tmpPtr = handle.AddrOfPinnedObject();
				//UPGRADE_WARNING: (8007) Trying to marshal a non Bittable Type (mbDirectorioActivo.SecBuffer). A special conversion might be required at this point. Moreover use 'External Marshalling attributes for Structs' feature enabled if required More Information: http://www.vbtonet.com/ewis/ewi8007.aspx
				UpgradeSupportHelper.PInvoke.UnsafeNative.kernel32.CopyMemory(tmpPtr, tmpPtr2, Length);
				Source = Marshal.ReadInt32(tmpPtr2);
			}
			finally
			{
				handle.Free();
				handle2.Free();
			}
		}

        public static void CopyMemory(out string Destination, IntPtr Source, int Length)
        {
            Destination = new string(' ', Length);
            IntPtr ipDest = Marshal.StringToBSTR(Destination);

            try
            {
                UpgradeSupportHelper.PInvoke.UnsafeNative.kernel32.CopyMemory(ipDest, Source, Length);
                Destination = Marshal.PtrToStringAnsi(Source);
            }
            finally
            {
                UpgradeSupportHelper.PInvoke.UnsafeNative.wtsapi32.WTSFreeMemory(ipDest);
                UpgradeSupportHelper.PInvoke.UnsafeNative.wtsapi32.WTSFreeMemory(Source);
            }
        }
        
		public static int GetVersionEx(ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.OSVERSIONINFO lpVersionInformation)
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.kernel32.GetVersionEx(ref lpVersionInformation);
		}
		public static int HeapAlloc(int hHeap, int dwFlags, int dwBytes)
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.kernel32.HeapAlloc(hHeap, dwFlags, dwBytes);
		}
		public static int HeapFree(int hHeap, int dwFlags, int lpMem)
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.kernel32.HeapFree(hHeap, dwFlags, lpMem);
		}
		public static int OpenProcess(int dwDesiredAccess, int bInheritHandle, int dwProcessId)
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.kernel32.OpenProcess(dwDesiredAccess, bInheritHandle, dwProcessId);
		}
		public static int ProcessFirst(int hSnapShot, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.PROCESSENTRY32 uProcess)
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.kernel32.ProcessFirst(hSnapShot, ref uProcess);
		}
		public static int ProcessIdToSessionId(int dwProcessId, ref int pSessionId)
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.kernel32.ProcessIdToSessionId(dwProcessId, ref pSessionId);
		}
		public static int ProcessNext(int hSnapShot, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.PROCESSENTRY32 uProcess)
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.kernel32.ProcessNext(hSnapShot, ref uProcess);
		}
		public static void RtlMoveMemory(int hpvDest, int hpvSource, int cbCopy)
		{
			GCHandle handle = GCHandle.Alloc(hpvDest, GCHandleType.Pinned);
			try
			{
				IntPtr tmpPtr = handle.AddrOfPinnedObject();
				UpgradeSupportHelper.PInvoke.UnsafeNative.kernel32.RtlMoveMemory(tmpPtr, hpvSource, cbCopy);
				hpvDest = Marshal.ReadInt32(tmpPtr);
			}
			finally
			{
				handle.Free();
			}
		}
		public static int TerminateProcess(int hProcess, int uExitCode)
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.kernel32.TerminateProcess(hProcess, uExitCode);
		}
		public static int WaitForSingleObject(int hHandle, int dwMilliseconds)
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.kernel32.WaitForSingleObject(hHandle, dwMilliseconds);
		}
		public static void CopyMemory<T, T2>(int Destination, ref T Source, int Length) where T : struct
		{
			GCHandle handle = GCHandle.Alloc(Destination, GCHandleType.Pinned);
			GCHandle handle2 = GCHandle.Alloc(Source, GCHandleType.Pinned);
			try
			{
				IntPtr tmpPtr2 = handle2.AddrOfPinnedObject();
				//UPGRADE_WARNING: (8007) Trying to marshal a non Bittable Type (mbDirectorioActivo.SecBuffer). A special conversion might be required at this point. Moreover use 'External Marshalling attributes for Structs' feature enabled if required More Information: http://www.vbtonet.com/ewis/ewi8007.aspx
				IntPtr tmpPtr = handle.AddrOfPinnedObject();
				UpgradeSupportHelper.PInvoke.UnsafeNative.kernel32.CopyMemory(tmpPtr, tmpPtr2, Length);
				Destination = Marshal.ReadInt32(tmpPtr);
			}
			finally
			{
				handle.Free();
				handle2.Free();
			}
		}
		public static int SetLocaleInfo(int Locale, int LCType, ref string lpLCData)
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.kernel32.SetLocaleInfo(Locale, LCType, ref lpLCData);
		}
		public static void Sleep(int dwMilliseconds)
		{
			UpgradeSupportHelper.PInvoke.UnsafeNative.kernel32.Sleep(dwMilliseconds);
		}
		public static int WriteProfileString(ref string lpszSection, ref string lpszKeyName, ref string lpszString)
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.kernel32.WriteProfileString(ref lpszSection, ref lpszKeyName, ref lpszString);
		}
        public static int GetLocaleInfo(int Locale, int LCType, ref string lpLCData, int cchData)
        {
            return UpgradeSupportHelper.PInvoke.UnsafeNative.kernel32.GetLocaleInfo(Locale, LCType, ref lpLCData, cchData);
        }
    }
}
using System;
using System.Runtime.InteropServices;

namespace UpgradeSupportHelper.PInvoke.SafeNative
{
    public static class secur32
	{

		public static int AcceptSecurityContext(ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecHandle phCredential, int phContext, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecBufferDesc pInput, int fContextReq, int TargetDataRep, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecHandle phNewContext, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecBufferDesc pOutput, ref int pfContextAttr, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.TimeStamp ptsExpiry)
		{
			int result = 0;
			GCHandle handle = GCHandle.Alloc(phContext, GCHandleType.Pinned);
			try
			{
				IntPtr tmpPtr = handle.AddrOfPinnedObject();
				result = UpgradeSupportHelper.PInvoke.UnsafeNative.secur32.AcceptSecurityContext(ref phCredential, tmpPtr, ref pInput, fContextReq, TargetDataRep, ref phNewContext, ref pOutput, ref pfContextAttr, ref ptsExpiry);
				phContext = Marshal.ReadInt16(tmpPtr);
			}
			finally
			{
				handle.Free();
			}
			return result;
		}
		public static int DeleteSecurityContext(ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecHandle phContext)
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.secur32.DeleteSecurityContext(ref phContext);
		}
		public static int AcquireCredentialsHandle<T>(int pszPrincipal, ref string pszPackage, int fCredentialUse, int pvLogonId, ref T pAuthData, int pGetKeyFn, int pvGetKeyArgument, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecHandle phCredential, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.TimeStamp ptsExpiry) where T : struct
		{
			int result = 0;
			GCHandle handle = GCHandle.Alloc(pAuthData, GCHandleType.Pinned);
			try
			{
				IntPtr tmpPtr = handle.AddrOfPinnedObject();
				//UPGRADE_WARNING: (8007) Trying to marshal a non Bittable Type (mbDirectorioActivo.SEC_WINNT_AUTH_IDENTITY). A special conversion might be required at this point. Moreover use 'External Marshalling attributes for Structs' feature enabled if required More Information: http://www.vbtonet.com/ewis/ewi8007.aspx
				result = UpgradeSupportHelper.PInvoke.UnsafeNative.secur32.AcquireCredentialsHandle(pszPrincipal, ref pszPackage, fCredentialUse, pvLogonId, tmpPtr, pGetKeyFn, pvGetKeyArgument, ref phCredential, ref ptsExpiry);
			}
			finally
			{
				handle.Free();
			}
			return result;
		}
		public static int FreeContextBuffer(int pvContextBuffer)
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.secur32.FreeContextBuffer(pvContextBuffer);
		}
		public static int QuerySecurityPackageInfo(ref string PackageName, ref int pPackageInfo)
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.secur32.QuerySecurityPackageInfo(ref PackageName, ref pPackageInfo);
		}
		public static int AcceptSecurityContext<T>(ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecHandle phCredential, ref T phContext, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecBufferDesc pInput, int fContextReq, int TargetDataRep, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecHandle phNewContext, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecBufferDesc pOutput, ref int pfContextAttr, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.TimeStamp ptsExpiry) where T : struct
		{
			int result = 0;
			GCHandle handle = GCHandle.Alloc(phContext, GCHandleType.Pinned);
			try
			{
				IntPtr tmpPtr = handle.AddrOfPinnedObject();
				//UPGRADE_WARNING: (8007) Trying to marshal a non Bittable Type (mbDirectorioActivo.SecHandle). A special conversion might be required at this point. Moreover use 'External Marshalling attributes for Structs' feature enabled if required More Information: http://www.vbtonet.com/ewis/ewi8007.aspx
				result = UpgradeSupportHelper.PInvoke.UnsafeNative.secur32.AcceptSecurityContext(ref phCredential, tmpPtr, ref pInput, fContextReq, TargetDataRep, ref phNewContext, ref pOutput, ref pfContextAttr, ref ptsExpiry);
			}
			finally
			{
				handle.Free();
			}
			return result;
		}
		public static int InitializeSecurityContext<T, T2, T3>(ref T phCredential, ref T2 phContext, int pszTargetName, int fContextReq, int Reserved1, int TargetDataRep, ref T3 pInput, int Reserved2, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecHandle phNewContext, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecBufferDesc pOutput, ref int pfContextAttr, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.TimeStamp ptsExpiry) where T : struct
		{
			int result = 0;
			GCHandle handle = GCHandle.Alloc(phCredential, GCHandleType.Pinned);
			GCHandle handle2 = GCHandle.Alloc(phContext, GCHandleType.Pinned);
			GCHandle handle3 = GCHandle.Alloc(pInput, GCHandleType.Pinned);
			try
			{
				IntPtr tmpPtr3 = handle3.AddrOfPinnedObject();
				//UPGRADE_WARNING: (8007) Trying to marshal a non Bittable Type (mbDirectorioActivo.SecBufferDesc). A special conversion might be required at this point. Moreover use 'External Marshalling attributes for Structs' feature enabled if required More Information: http://www.vbtonet.com/ewis/ewi8007.aspx
				IntPtr tmpPtr2 = handle2.AddrOfPinnedObject();
				//UPGRADE_WARNING: (8007) Trying to marshal a non Bittable Type (mbDirectorioActivo.SecHandle). A special conversion might be required at this point. Moreover use 'External Marshalling attributes for Structs' feature enabled if required More Information: http://www.vbtonet.com/ewis/ewi8007.aspx
				IntPtr tmpPtr = handle.AddrOfPinnedObject();
				result = UpgradeSupportHelper.PInvoke.UnsafeNative.secur32.InitializeSecurityContext(tmpPtr, tmpPtr2, pszTargetName, fContextReq, Reserved1, TargetDataRep, tmpPtr3, Reserved2, ref phNewContext, ref pOutput, ref pfContextAttr, ref ptsExpiry);
			}
			finally
			{
				handle.Free();
				handle2.Free();
				handle3.Free();
			}
			return result;
		}
		public static int AcquireCredentialsHandle(int pszPrincipal, ref string pszPackage, int fCredentialUse, int pvLogonId, int pAuthData, int pGetKeyFn, int pvGetKeyArgument, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecHandle phCredential, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.TimeStamp ptsExpiry)
		{
			int result = 0;
			GCHandle handle = GCHandle.Alloc(pAuthData, GCHandleType.Pinned);
			try
			{
				IntPtr tmpPtr = handle.AddrOfPinnedObject();
				result = UpgradeSupportHelper.PInvoke.UnsafeNative.secur32.AcquireCredentialsHandle(pszPrincipal, ref pszPackage, fCredentialUse, pvLogonId, tmpPtr, pGetKeyFn, pvGetKeyArgument, ref phCredential, ref ptsExpiry);
				pAuthData = Marshal.ReadInt16(tmpPtr);
			}
			finally
			{
				handle.Free();
			}
			return result;
		}
		public static int InitializeSecurityContext<T, T2, T3>(ref T phCredential, int phContext, int pszTargetName, int fContextReq, int Reserved1, int TargetDataRep, int pInput, int Reserved2, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecHandle phNewContext, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecBufferDesc pOutput, ref int pfContextAttr, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.TimeStamp ptsExpiry) where T : struct
		{
			int result = 0;
			GCHandle handle = GCHandle.Alloc(phCredential, GCHandleType.Pinned);
			GCHandle handle2 = GCHandle.Alloc(phContext, GCHandleType.Pinned);
			GCHandle handle3 = GCHandle.Alloc(pInput, GCHandleType.Pinned);
			try
			{
				IntPtr tmpPtr3 = handle3.AddrOfPinnedObject();
				IntPtr tmpPtr2 = handle2.AddrOfPinnedObject();
				IntPtr tmpPtr = handle.AddrOfPinnedObject();
				//UPGRADE_WARNING: (8007) Trying to marshal a non Bittable Type (mbDirectorioActivo.SecHandle). A special conversion might be required at this point. Moreover use 'External Marshalling attributes for Structs' feature enabled if required More Information: http://www.vbtonet.com/ewis/ewi8007.aspx
				result = UpgradeSupportHelper.PInvoke.UnsafeNative.secur32.InitializeSecurityContext(tmpPtr, tmpPtr2, pszTargetName, fContextReq, Reserved1, TargetDataRep, tmpPtr3, Reserved2, ref phNewContext, ref pOutput, ref pfContextAttr, ref ptsExpiry);
				pInput = Marshal.ReadInt16(tmpPtr3);
				phContext = Marshal.ReadInt16(tmpPtr2);
			}
			finally
			{
				handle.Free();
				handle2.Free();
				handle3.Free();
			}
			return result;
		}
		public static int CompleteAuthToken(ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecHandle phContext, ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecBufferDesc pToken)
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.secur32.CompleteAuthToken(ref phContext, ref pToken);
		}
		public static int FreeCredentialsHandle(ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.SecHandle phContext)
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.secur32.FreeCredentialsHandle(ref phContext);
		}
	}
}
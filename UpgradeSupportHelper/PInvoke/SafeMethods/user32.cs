using System;

namespace UpgradeSupportHelper.PInvoke.SafeNative
{
    public static class user32
	{

		public static int FindWindow(ref string lpClassName, ref string lpWindowName)
		{
			return (int)UpgradeSupportHelper.PInvoke.UnsafeNative.user32.FindWindow(ref lpClassName, ref lpWindowName);
		}
		public static int GetCursorPos(ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.POINTAPI lpPoint)
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.user32.GetCursorPos(ref lpPoint);
		}
		public static int SendMessage(int hwnd, int wMsg, int wParam, ref string lParam)
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.user32.SendMessage(hwnd, wMsg, wParam, ref lParam);
		}
		public static int SendMessage(int hwnd, int wMsg, int wParam, ref int lParam)
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.user32.SendMessage(hwnd, wMsg, wParam, ref lParam);
		}
        public static int CallNextHookEx(int hHook, int nCode, int wParam, int lparam)
        {
            return UpgradeSupportHelper.PInvoke.UnsafeNative.user32.CallNextHookEx(hHook, nCode, wParam, lparam);
        }
        public static int GetAsyncKeyState(int vKey)
        {
            return UpgradeSupportHelper.PInvoke.UnsafeNative.user32.GetAsyncKeyState(vKey);
        }


        public static int SetWindowPos(int hwnd, 
                                       int hWndInsertAfter, 
                                       int x, int y, int cx, int cy, 
                                       int wFlags)
        {
            UpgradeSupportHelper.PInvoke.UnsafeNative.Enumerations.SetWindowPosFlags flags =
                (UpgradeSupportHelper.PInvoke.UnsafeNative.Enumerations.SetWindowPosFlags)wFlags;

            return UpgradeSupportHelper.PInvoke.UnsafeNative.user32.SetWindowPos(new IntPtr(hwnd), new IntPtr(hWndInsertAfter), x, y, cx, cy, flags);
        }
        public static int SetWindowsHookEx(int idHook, int lpfn, int hMod, int dwThreadId)
        {
            return UpgradeSupportHelper.PInvoke.UnsafeNative.user32.SetWindowsHookEx(idHook, lpfn, hMod, dwThreadId);
        }
        public static int UnhookWindowsHookEx(int hHook)
        {
            return UpgradeSupportHelper.PInvoke.UnsafeNative.user32.UnhookWindowsHookEx(hHook);
        }
        public static int GetWindowText(int hwnd, ref string lpString, int cch)
        {
            return UpgradeSupportHelper.PInvoke.UnsafeNative.user32.GetWindowText(hwnd, ref lpString, cch);
        }
        public static int EnumWindows(int wndenmprc, int lparam)
        {
            return UpgradeSupportHelper.PInvoke.UnsafeNative.user32.EnumWindows(wndenmprc, lparam);
        }
        public static void keybd_event(byte bVk, byte bScan, int dwFlags, int dwExtraInfo)
        {
            UpgradeSupportHelper.PInvoke.UnsafeNative.user32.keybd_event(bVk, bScan, dwFlags, dwExtraInfo);
        }

        public static IntPtr SetTimer(IntPtr hWnd, IntPtr nIDEvent, uint uElapse, IntPtr lpTimerFunc)
        {
            return UpgradeSupportHelper.PInvoke.UnsafeNative.user32.SetTimer(hWnd, nIDEvent, uElapse, lpTimerFunc);
        }

        public static int KillTimer(IntPtr hWnd, IntPtr nIDEvent)
        {
            return UpgradeSupportHelper.PInvoke.UnsafeNative.user32.KillTimer(hWnd, nIDEvent);
        }
    }
}
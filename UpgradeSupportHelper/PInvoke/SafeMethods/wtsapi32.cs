using System;

namespace UpgradeSupportHelper.PInvoke.SafeNative
{
    public static class wtsapi32
	{

        public static int WTSQuerySessionInformation(IntPtr hServer, int SessionID, int WTSInfoClass, out IntPtr ppBuffer, out int pBytesReturned)
		{
			return UpgradeSupportHelper.PInvoke
                                       .UnsafeNative
                                       .wtsapi32
                                       .WTSQuerySessionInformation(hServer, 
                                                                   SessionID, 
                                                                   WTSInfoClass, 
                                                                   out ppBuffer,
                                                                   out pBytesReturned);
		}

        public static void WTSFreeMemory(IntPtr memory)
        {
            UpgradeSupportHelper.PInvoke.UnsafeNative.wtsapi32.WTSFreeMemory(memory);
        }
	}
}
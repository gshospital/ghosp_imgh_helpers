namespace UpgradeSupportHelper.PInvoke.SafeNative
{
    public static class ws2_32
	{

		public static int gethostbyname(ref string host_name)
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.ws2_32.gethostbyname(ref host_name);
		}
		public static int gethostname(ref string host_name, int namelen)
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.ws2_32.gethostname(ref host_name, namelen);
		}
	}
}
namespace UpgradeSupportHelper.PInvoke.SafeNative
{
    public static class wininet
	{

		public static int InternetOpen(ref string sAgent, int lAccessType, ref string sProxyName, ref string sProxyBypass, int lFlags)
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.wininet.InternetOpen(ref sAgent, lAccessType, ref sProxyName, ref sProxyBypass, lFlags);
		}
	}
}
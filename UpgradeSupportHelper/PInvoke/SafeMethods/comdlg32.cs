﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpgradeSupportHelper.PInvoke.SafeNative
{
    public class comdlg32
    {
        public static int GetOpenFileName(ref UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.OPENFILENAME pOpenfilename)
        {
            return UpgradeSupportHelper.PInvoke.UnsafeNative.comdlg32.GetOpenFileName(ref pOpenfilename);
        }
    }
}

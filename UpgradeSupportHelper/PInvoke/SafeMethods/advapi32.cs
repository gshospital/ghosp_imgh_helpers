using System;
using System.Runtime.InteropServices;

namespace UpgradeSupportHelper.PInvoke.SafeNative
{
    public static class advapi32
	{

		public static int GetUserName(ref string lpbuffer, ref int nSize)
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.advapi32.GetUserName(ref lpbuffer, ref nSize);
		}
        public static int RegOpenKeyEx(int hKey, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpSubKey, int ulOptions, int samDesired, ref int phkResult)
        {
            return UpgradeSupportHelper.PInvoke.UnsafeNative.advapi32.RegOpenKeyEx(hKey, ref lpSubKey, ulOptions, samDesired, ref phkResult);
        }
        public static int RegCloseKey(int hKey)
        {
            return UnsafeNative.advapi32.RegCloseKey(hKey);
        }

        public static int RegQueryValueEx(int hKey, ref string lpValueName, int lpReserved, ref int lpType, int lpData, ref int lpcbData)
        {
            int result = 0;
            GCHandle handle = GCHandle.Alloc(lpData, GCHandleType.Pinned);
            try
            {
                IntPtr tmpPtr = handle.AddrOfPinnedObject();
                result = UnsafeNative.advapi32.RegQueryValueEx(hKey, ref lpValueName, lpReserved, ref lpType, tmpPtr, ref lpcbData);
                lpData = Marshal.ReadInt16(tmpPtr);
            }
            finally
            {
                handle.Free();
            }
            return result;
        }
        public static int RegQueryValueEx(int hKey, ref string lpValueName, int lpReserved, ref int lpType, string lpData, ref int lpcbData)
        {
            int result = 0;
            IntPtr tmpPtr = Marshal.StringToHGlobalAnsi(lpData);
            try
            {
                result = UnsafeNative.advapi32.RegQueryValueEx(hKey, ref lpValueName, lpReserved, ref lpType, tmpPtr, ref lpcbData);
                lpData = Marshal.PtrToStringAnsi(tmpPtr);
            }
            finally
            {
                Marshal.FreeHGlobal(tmpPtr);
            }
            return result;
        }
    }
}
namespace UpgradeSupportHelper.PInvoke.SafeNative
{
    public static class odbccp32
	{

		public static int SQLConfigDataSource(int hwndParent, int fRequest, ref string lpszDriver, ref string lpszAttributes)
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.odbccp32.SQLConfigDataSource(hwndParent, fRequest, ref lpszDriver, ref lpszAttributes);
		}
	}
}
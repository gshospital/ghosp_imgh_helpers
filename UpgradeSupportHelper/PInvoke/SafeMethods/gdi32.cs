using System;
using System.Runtime.InteropServices;

namespace UpgradeSupportHelper.PInvoke.SafeNative
{
    public static class gdi32
	{

		public static int Escape(int hdc, int nEscape, int nCount, ref string lpInData, int lpOutData)
		{
			int result = 0;
			GCHandle handle = GCHandle.Alloc(lpOutData, GCHandleType.Pinned);
			try
			{
				IntPtr tmpPtr = handle.AddrOfPinnedObject();
				result = UpgradeSupportHelper.PInvoke.UnsafeNative.gdi32.Escape(hdc, nEscape, nCount, ref lpInData, tmpPtr);
				lpOutData = Marshal.ReadInt16(tmpPtr);
			}
			finally
			{
				handle.Free();
			}
			return result;
		}
        public static int BitBlt(int hDestDC, int X, int Y, int nWidth, int nHeight, int hSrcDC, int xSrc, int ySrc, int dwRop)
        {
            return PInvoke.UnsafeNative.gdi32.BitBlt(hDestDC, X, Y, nWidth, nHeight, hSrcDC, xSrc, ySrc, dwRop);
        }
    }
}
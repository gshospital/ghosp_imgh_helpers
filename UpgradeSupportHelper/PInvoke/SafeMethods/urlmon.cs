namespace UpgradeSupportHelper.PInvoke.SafeNative
{
    public static class urlmon
	{

		public static int URLDownloadToFile(int pCaller, ref string szURL, ref string szFileName, int dwReserved, int lpfnCB)
		{
			return UpgradeSupportHelper.PInvoke.UnsafeNative.urlmon.URLDownloadToFile(pCaller, ref szURL, ref szFileName, dwReserved, lpfnCB);
		}
	}
}